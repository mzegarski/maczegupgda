package date;

public class MyDate {

	private int year, month, day = 0;

	private String[] strMonths = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	private String[] strDay = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

	private int daysInMonths[] = { 31, 28, 31, 30, 30, 31, 31, 30, 31, 30, 31 };

	public MyDate(int day, int month, int year) {

		super();
		setYear(year);
		setMonth(month);
		setDay(day);
	}

	public boolean isLeapYear(int year) {
		if (year % 4 == 0)
			return true;
		else
			return false;
	}

	public boolean isValidDate(int year, int month, int day) {

		 // tutaj podj�c
		
	//	if (!(month == 1 && day == 29 && isLeapYear(year) == true)) return false;
		if (day > daysInMonths[month - 1]) return false;
//		if (!( day >= 1 && day <= 31)) 		 return false;
//		if (!( month >=1 && month <=12)) return false;
//		if (!(year >=1)) return false;
		return true;
	}

	// public int getDayOfWeek(int year, int month, int day) {}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {

		if (year > 0)
			this.year = year;
	}

	public int getMonth() {

		return month;
	}

	public void setMonth(int month) {

		if (month >= 1 && month <= 12)
			this.month = month;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {

		this.day = day;
	}

	@Override
	public String toString() {

		if (isValidDate(year, month, day) == false)
			return "Wpisana data jest niepoprawna";
		else
			return "Data to: " + day + " " + strMonths[month - 1] + " " + year;
	}

}
