package time;

public class MyTime {

	private int hour = 0;
	private int minute = 0;
	private int second = 0;

	public MyTime(int hour, int minute, int second) {
		super();
		setHour(hour);
		setMinute(minute);
		setSecond(second);
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		if (hour > 0 && hour < 24) {
			this.hour = hour;
		}

	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		if (minute > 0 && minute < 60) {
			this.minute = minute;
		}

	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		if (second > 0 && second < 60)
			this.second = second;
	}

	public MyTime nextHour() {
		int newHour = this.hour + 1;
		if (newHour == 24) {
			newHour = 0;
		}
		return new MyTime(newHour, this.minute, this.second);
	}

	public MyTime nextMinute() {
		int newMinute = this.minute + 1;
		int newHour = this.hour;
		if (newMinute == 60) {
			newMinute = 0;
			newHour++;

		}
		return new MyTime(newHour, newMinute, this.second);

	}

	public MyTime nextSecond() {

		int newSecond = this.second + 1;
		int newMinute = this.minute;
		int newHour = this.hour;
		if (newSecond == 60) {
			newSecond = 0;
			newMinute++;
		}
		if (newMinute == 60) {
			newMinute = 0;
			newHour++;

		}
		return new MyTime(newHour, newMinute, newSecond);

	}

	private int subtractHour(int newHour) {
		if (newHour < 0) {
			newHour = 23;
		}
		return newHour;
	}

	
	
	public MyTime previousHour() {
		int newHour = this.hour - 1;

		newHour = subtractHour(newHour);
		return new MyTime(newHour, this.minute, this.second);
	}

	public MyTime previousMinute() {
		int newMinute = this.minute - 1;
		int newHour = this.hour;

		if (newMinute < 0) {
			newMinute = 59;
			newHour--;
		}
		
		newHour = subtractHour(newHour);
		return new MyTime(newHour, newMinute, this.second);
	}

	public MyTime previousSecond() {
		int newSecond = this.second - 1;
		int newMinute = this.minute;
		int newHour = this.hour;
		{
			if (newSecond < 0) {
				newSecond = 59;
				newMinute--;
			}
			if (newMinute < 0) {
				newMinute = 59;
				newHour--;
			}
			newHour = subtractHour(newHour);
		}
		return new MyTime(newHour, newMinute, newSecond);
	}

	@Override
	public String toString() {

		return "Czas to: " + leadZero(this.hour) + ":" + leadZero(this.minute) + ":" + leadZero(this.second);

	}

	public String leadZero(int number) {
		if (number < 10) {
			return "0" + number;
		} else {
			return "" + number;
		}

	}

}
