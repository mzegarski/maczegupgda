package main;

import java.util.LinkedList;
import java.util.List;

import calculator.Calc;
import calculator.CalcInterface;

public class Main {

	public static void main(String[] args) {

		Thread t = new Thread(new ThreadExampleOne());

		t.start();

		new Thread(new ThreadExampleTwo(), "Jakis watek").start();
		System.out.println("KONIEC");

		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Pochodze z kodu Runnable");
			}
		}).start();

		new Thread(() -> {
			int a = 1;
			int b = 3;

			System.out.println("Suma wynosi: " + (a + b));

		}).start(); // skrocony zapis implementacji kodu metody w interface
					// funkcyjnym

		new Thread(() -> System.out.println("Wiadomosc z FI")).start();

		List<Person> p = new LinkedList<>();

		p.add(new Person(1));

		Calc c = new Calc();

		// System.out.println(c.add(4, 3));

		System.out.println(c.oper(5, 6, new CalcInterface() {

			@Override
			public double doOperation(int a, int b) {

				return a + b;
			}
		}));

		System.out.println(c.oper(17, 3, (a, b) -> (a - b)));
		
		System.out.println(c.oper(17, 3, (a, b) -> (a * b)));

	}

}
