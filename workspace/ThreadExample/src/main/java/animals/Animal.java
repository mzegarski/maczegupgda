package animals;

import java.util.LinkedList;
import java.util.List;

public class Animal {

	private List<String> animals = new LinkedList<>();

	public Animal() {
	}

	public void addAnimal(String animal, AnimalInterface ai) {

		String[] anims = ai.make(animal);
		for (String a : anims) {
			animals.add(a);
		}
	}

	public String getAnimal(int index) {

		return (animals.size() > index) ? animals.get(index) : "";

	}

	public boolean containsAnimal(String animal, AnimalInterface ai) {

		String[] exist = ai.make(animal);

		for (String a : exist) {
			if (!animals.contains(a));
			return false;
		}

		return true;

	}

}
