package animals;

public class Main {

	public static void main(String[] args) {
		Animal a = new Animal();
		
		a.addAnimal("zolw", (anim)-> anim.split(" "));
		
		a.addAnimal("slon|kot", (anim) -> anim.split("\\|"));
		
		System.out.println(a.containsAnimal("zolw", (s) -> s.split("")));
		
	}

}
