package employee;

import java.util.LinkedList;
import java.util.List;

public class Company {

	private List<Employee> employees = new LinkedList<>();

	public void filter(FilterInterface f, TransformInterface t) {

		
		
		for (Employee e : employees) {

			if (f.test(e.getName()) == true ) {

				System.out.println(t.transform(e.getName()));
				
				

			}
		}

	}

	public void add(String name, String lastname, int age, int salary) {

		employees.add(new Employee(name, lastname, age, salary));

	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

}
