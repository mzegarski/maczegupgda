package employee;

import java.util.LinkedList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		Company company = new Company();

		company.add("Paweł", "Testowy", 18, 3000);
		company.add("Piotr", "Przykladowy", 32, 10000);
		company.add("Julia", "Doe", 41, 4300);
		company.add("Przemysław", "Wietrak", 56, 4500);
		company.add("Zofia", "Zaspa", 37, 3700);

		company.filter(s -> s.startsWith("P"), s -> s);

	}

}
