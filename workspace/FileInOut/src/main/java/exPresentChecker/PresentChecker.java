package exPresentChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class PresentChecker {

	private String filename = "words.txt";
	private String path = "src/main/resources/";

	private HashMap<Integer, String> wordsMap = new HashMap<>();

	public PresentChecker() {

	}

	public PresentChecker(String filename) {
		this.filename = filename;
	}

	private HashMap<Integer, String> readAndSaveToHashMap() {

		File file = new File(path + filename);

		try (Scanner scan = new Scanner(file)) {
			int i = 0;
			while (scan.hasNextLine()) {
				wordsMap.put(i++, scan.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NO FILE FOUND");
		}

		return wordsMap;
	}

	private boolean checkIfExists(String sentence) {

		return wordsMap.containsValue(sentence);

	}

	public void readWords() {

		readAndSaveToHashMap();

		Scanner sc = new Scanner(System.in);
		String word = "";
		System.out.println("Program sprawdza czy wpisane s�owo znajduje si� w pliku " + filename);
		do {
			System.out.println("Podaj s�owo [wpisz \"exit\" �eby zako�czy�]: ");
			word = sc.nextLine();

			if (word.equals("exit"))
				break;
			if (checkIfExists(word) == true)
				System.out.println("S�owo \"" + word + "\" znajduje si� pliku " + filename);
			else
				System.out.println("S�owo \"" + word + "\" nie znajduje si� pliku " + filename);

		} while (word != "exit");

		System.out.println("Ko�cz� program.");

		sc.close();

	}

}
