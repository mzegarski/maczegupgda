package exTemperature;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class TempConventer {

	private String filename = "src/main/resources/tempC.txt";

	public double toKelvin(double temp) {

		// �K = �C + 273.15
		return temp + 273.15;

	}

	public double toFahrenheir(double temp) {

		// �F = (�C � 1.8) + 32

		return (temp * 1.8) + 32;
	}

	// public void writeTemp(double[] temp) {
	//
	// String arrayName = "" + temp.getClass().getName();
	//
	// File f = new File("src/main/resources/" + arrayName);
	//
	// try {
	// FileOutputStream fos = new FileOutputStream(f);
	//
	// PrintWriter pw = new PrintWriter(fos);
	//
	// for (double d : temp) {
	// pw.println(d);
	// }
	//
	// pw.close();
	//
	// } catch (FileNotFoundException e) {
	// System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
	//
	// }
	//
	// }

	public void writeTemp(double[] temp) {
		File toK = new File("src/main/resources/tempK.txt");
		File toF = new File("src/main/resources/tempF.txt");

		try {
			FileOutputStream fosK = new FileOutputStream(toK);
			FileOutputStream fosF = new FileOutputStream(toF);

			PrintWriter pwK = new PrintWriter(fosK);
			PrintWriter pwF = new PrintWriter(fosF);

			for (double c : temp) {
				pwK.println(toKelvin(c));
				pwF.println(toFahrenheir(c));
			}

			pwK.close();
			pwF.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public double[] readTemp() {

		File f = new File(filename);
		double[] tempC = new double[countLines()];

		try {

			Scanner sc = new Scanner(f);

			int i = 0;
			while (sc.hasNextLine()) {
				tempC[i++] = Double.parseDouble(sc.nextLine().replace(",", "."));
			}

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}

		return tempC;

	}

	public int countLines() {

		File f = new File(filename);
		int n = 0;
		try {
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				n++;
				sc.nextLine();
			}
			sc.close();

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}

		return n;

	}

}
