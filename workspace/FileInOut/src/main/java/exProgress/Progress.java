package exProgress;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Progress {

	private String filename = "progress.txt";
	private String path = "src/main/resources/";

	private HashMap<String, Integer> progressMap = new HashMap<>();

	public Progress() {
		readDict();
	}

	public void readDict() {
		File file = new File(path + filename);
		try (Scanner scan = new Scanner(file)) {

			String currentLine = "";
			while (scan.hasNextLine()) {
				currentLine = scan.nextLine();
				String temp[] = currentLine.split("\t");
				progressMap.put(temp[0], Integer.parseInt(temp[1]));
			}
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NO FILE FOUND");
		}
	}

	public void printProgressinLine() {

		int n = 0;

		for (Map.Entry<String, Integer> entry : progressMap.entrySet()) {

			System.out.print(++n + " |");
			for (int i = 0; i < (entry.getValue() / 10); i++) {
				System.out.print("#");
			}
			System.out.println(" (" + entry.getValue() + "%)");

		}

		System.out.println();
		n = 0;

		for (Map.Entry<String, Integer> entry : progressMap.entrySet()) {

			System.out.println(++n + " - " + entry.getKey());

		}

	}

	public void printProgressinColumn() {

		System.out.println(progressMap.size());

		for (int i = 0; i < progressMap.size(); i++) {
			System.out.print((i + 1) + " ");
		}
		System.out.println();

		for (int i = 0; i < progressMap.size(); i++) {
			System.out.print("- ");
		}

		System.out.println();

		for (Map.Entry<String, Integer> entry : progressMap.entrySet()) {

			for (int i = 0; i < (entry.getValue() / 10); i++) {
				System.out.print("#");
			}
			System.out.println(" (" + entry.getValue() + "%)");

		}

	}

}
