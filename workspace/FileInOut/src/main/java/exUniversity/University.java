package exUniversity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

//Warto�ci powinny by� rozdzielone znakiem tabulacji "\t".
//Metoda getStudent() powinna zwraca� now� instancj� klasy Student
//z uzupe�nionymi danymi studenta wed�ug indeksu,
//a metoda putStudent() dodawa� studenta na ko�cu listy,
//w przypadku, gdy taki student nie istnieje.
//Do sprawdzenia wyst�powania studenta o zadanym indeksie wykorzystaj prywatn� metod� isStudentExists().

public class University extends Student {

	private String path = "src/main/resources/students.txt";

	private boolean isStudentExist(int index) {

		File f = new File(path);

		try (Scanner sc = new Scanner(f)) {
			String currentLine = "";

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] temp = currentLine.split("\t");
				if (Integer.parseInt(temp[0]) == index)
					return true;
			}

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}

		return false;
	}

	public Student getStudent(int index) {

		File f = new File(path);

		if (isStudentExist(index) == true) {

			try (Scanner sc = new Scanner(f)) {

				String currentLine = "";

				while (sc.hasNextLine()) {
					currentLine = sc.nextLine();
					String[] temp = currentLine.split("\t");
					if (Integer.parseInt(temp[0]) == index)
						return new Student(index, temp[1], temp[2], temp[3], Double.parseDouble(temp[4]));
				}
			} catch (FileNotFoundException e) {
				System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
			}

		}
		return null;
	}

}
