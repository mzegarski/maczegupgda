package pl.org.pfig.exercises;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		// readAndWrite();

		// currencyConventerSimple();

		MoneyConventer m = new MoneyConventer();

		System.out.println(m.convert(100, "USD"));
		System.out.println(m.convert(100, "USD", "EUR"));

	}

	private static void readAndWrite() {
		Sentencer s = new Sentencer();

		String mySentence = s.readSentence("test.txt");

		s.writeSentence("test2.txt", mySentence);
	}

	private static void currencyConventerSimple() {
		String myStr = "1 CNY	0,6064";
		String[] temp = myStr.split("\t");
		String[] values = temp[0].split(" ");

		int howMany = Integer.parseInt(values[0]);
		String currency = values[1].trim();
		double course = Double.parseDouble(temp[1].replace(",", "."));

		System.out.println(howMany + " | " + currency + " | " + course);
	}

}
