package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Sentencer extends Sentence {

	public void writeSentence(String filename, String sentence) {

		File f = new File("src/main/resources/" + filename);

		try {
			// je�eli chcemy odpisywac odp liku (tryb append), to musimy
			// przekazac drugi parametr do konstruktora FOS np:
			// FileOutputStrem fos = new FileOutputStream(f, true);
			FileOutputStream fos = new FileOutputStream(f);

			PrintWriter pw = new PrintWriter(fos);

			pw.println(sentence);
			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");

		}

	}

}
