package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MoneyConventer {

	private String filename = "src/main/resources/currency.txt";

	private double readCourse(String currency) {

		File f = new File(filename);
		try {
			String currentLine = "";
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				currentLine = sc.nextLine();
				String[] temp = currentLine.split("\t");
				String[] values = temp[0].split(" ");

				if (values[1].equalsIgnoreCase(currency)) {
					return Double.parseDouble(temp[1].replace(",", ".")) / Integer.parseInt(values[0]);
				}
			}
			sc.close();
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}
		return 0;
	}

	public double convert(double money, String to) {

		return fourDigits((money / readCourse(to)));

	}

	public double convert(double money, String to, String from) {

		return (readCourse(from) * convert(money, to));
	}

	public double fourDigits(double money) {

		java.text.DecimalFormat x = new java.text.DecimalFormat("0.0000");

		return Double.parseDouble(x.format(money).replace(",", "."));
	}

}
