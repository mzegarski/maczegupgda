package pl.org.pfig.exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Sentence {

	public String readSentence(String filename) {

		String sentence = "";
		File f = new File("src/main/resources/" + filename);

		try {
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				sentence += sc.nextLine() + " ";
			}

			sc.close();

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}

		sentence = "" + sentence.toUpperCase().charAt(0) + sentence.substring(1, sentence.length() - 1);
		if (!sentence.endsWith(".") ) {
			sentence += ".";
		}

		return sentence;
	}
	
	public void writeSentence(String filename, String sentence){
		
	}

}
