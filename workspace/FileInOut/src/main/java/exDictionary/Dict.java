package exDictionary;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Dict {

	private String filename = "dictionary.txt";
	private String path = "src/main/resources/";

	private HashMap<String, String> dictPolishToEnglish = new HashMap<>();

	public Dict() {
		readDict(filename);
	}

	public void readDict(String filename) {
		File file = new File(path + filename);

		try (Scanner scan = new Scanner(file)) {

			String currentLine = "";

			while (scan.hasNextLine()) {

				currentLine = scan.nextLine();

				String temp[] = currentLine.split("\t");
				dictPolishToEnglish.put(temp[0], temp[1]);
			}
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NO FILE FOUND");
		}

	}

	public void addWord(String polishWord, String englishWord) {

		if (dictPolishToEnglish.containsKey(polishWord) == true) {
			System.out.println("S�owo " + polishWord + " istnieje ju� w s�owniku i jest przypisane do t�umaczenia "
					+ dictPolishToEnglish.get(polishWord));

		} else {
			File toF = new File(path + filename);

			try {
				FileOutputStream fos = new FileOutputStream(toF, true);
				PrintWriter pw = new PrintWriter(fos);

				pw.println(polishWord + "\t" + englishWord);

				pw.close();

			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}

			readDict(filename);
			System.out.println("S�owo " + polishWord + " poprawnie przypisane do t�umaczenia " + englishWord);
		}
	}

	public void removeWord(String polishWord) {

		if (dictPolishToEnglish.containsKey(polishWord) == true) {

			try {

				dictPolishToEnglish.remove(polishWord);
				File toF = new File(path + filename);
				FileOutputStream fos = new FileOutputStream(toF);

				PrintWriter pw = new PrintWriter(fos);
				for (Map.Entry<String, String> entry : dictPolishToEnglish.entrySet()) {

					pw.println("" + entry.getKey() + "\t" + entry.getValue());
				}
				pw.close();
				System.out.println("Usuni�to s�owo " + polishWord + " z s�ownika.");
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}

		} else {
			System.out.println("Nie ma s�owa " + polishWord + " w s�owniku.");
		}

		readDict(filename);

	}

	public String translateToEn(String polishContent) {

		System.out.println("T�umacz� " + polishContent);

		if (dictPolishToEnglish.containsKey(polishContent)) {
			return polishContent + " = " + dictPolishToEnglish.get(polishContent);
		} else
			return "Brak t�umaczenia dla s�owa " + polishContent;

	}

	public String translatetoPl(String englishContent) {

		System.out.println("T�umacz� " + englishContent);

		for (Map.Entry<String, String> entry : dictPolishToEnglish.entrySet()) {

			if (entry.getValue().equals(englishContent)) {
				return englishContent + " = " + entry.getKey();
			}

		}
		return "Brak t�umaczenia dla s�owa " + englishContent;

	}

	public void printDictionary() {
		System.out.println("Drukuj� s�ownik: ");
		for (Map.Entry<String, String> entry : dictPolishToEnglish.entrySet()) {

			System.out.println("" + entry.getKey() + " = " + entry.getValue());
		}
	}

}
