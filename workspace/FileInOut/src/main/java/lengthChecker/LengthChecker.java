package lengthChecker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class LengthChecker {

	private String path = "src/main/resources/";

	private boolean isProperLength(String arg, int len) {

		if (arg.length() > len)
			return true;
		else
			return false;
	}

	private String[] readFile(String filename) {

		String[] fileRead = new String[countLines(filename)];
		File file = new File(path + filename);

		try {
			Scanner sc = new Scanner(file);
			int i = 0;
			while (sc.hasNextLine()) {
				fileRead[i++] = sc.nextLine();
			}
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NO FILE FOUND");
		}

		return fileRead;
	}

	private void writeFile(String[] filecontent, int len) {

		File toF = new File(path + "words_" + len + ".txt");
		try {
			FileOutputStream fos = new FileOutputStream(toF);
			PrintWriter pw = new PrintWriter(fos);

			for (String s : filecontent) {
				pw.println(s);
			}

			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

	public void make(String fileInput, int len) {

		int n = 0;

		String[] fileRead = readFile(fileInput);

		for (int i = 0; i < fileRead.length; i++)
			if (isProperLength(fileRead[i], len) == true)
				n++;

		if (n > 0) {

			String[] filecontent = new String[n];
			for (int i = 0, j = 0; i < fileRead.length; i++)
				if (isProperLength(fileRead[i], len) == true)
					filecontent[j++] = fileRead[i];

			writeFile(filecontent, len);
		}
	}

	public int countLines(String filename) {

		File f = new File(path + filename);
		int n = 0;
		try {
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				n++;
				sc.nextLine();
			}
			sc.close();

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}

		return n;
	}
}
