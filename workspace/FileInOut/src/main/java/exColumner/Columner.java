package exColumner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Columner {

	// +writeColumn(filename:String):void

	// Metoda writeColumn() powinna zapisywa� warto�ci wybranej kolumny do
	// pliku. W przypadku braku danej kolumny (ilo�� kolumn w pliku mniejsza ni�
	// przekazany argument) wyrzu� wyj�tek IllegalArgumentException.

	private String filename;
	private double[] currentColumn = new double[0];

	private String path = "src/main/resources/";

	public Columner() {

	}

	public Columner(String filename) {

		this.filename = filename;

	}

	public double[] readColumn(int column) {
		if (currentColumn.length > 0) {
			return currentColumn;
		} else {
			double[] columnRead = new double[countLines(filename)];
			String currentLine = "";

			File file = new File(path + filename);

			try (Scanner sc = new Scanner(file)) {

				int i = 0;
				while (sc.hasNextLine()) {
					currentLine = sc.nextLine();
					String[] temp = currentLine.replaceAll(",", ".").split("\t");
					columnRead[i++] = Double.parseDouble(temp[column - 1]);

				}
			} catch (FileNotFoundException e) {
				System.out.println("[ERROR] NO FILE FOUND");
			}
			currentColumn = columnRead;
			return columnRead;
		}

	}

	public int countLines(String filename) {

		File f = new File(path + filename);
		int n = 0;
		try {
			Scanner sc = new Scanner(f);

			while (sc.hasNextLine()) {
				n++;
				sc.nextLine();
			}
			sc.close();

		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NIE MA TAKIEGO PLIKU");
		}

		return n;
	}

	public double sumColumn(int column) {

		double sum = 0;

		for (double d : readColumn(column)) {
			sum += d;
		}

		return sum;
	}

	public double avgColumn(int column) {

		return sumColumn(column) / countColumn(column);

	}

	public int countColumn(int column) {

		return countLines(filename);
	}

	public double maxColumn(int column) {

		double max = readColumn(column)[0];

		for (double d : readColumn(column)) {
			if (max < d)
				max = d;
		}

		return max;

	}

	public double minColumn(int column) {

		double min = readColumn(column)[0];

		for (double d : readColumn(column)) {
			if (min > d)
				min = d;
		}

		return min;

	}

	public void writeColumn(String saveToFilename) {

		File toF = new File(path + saveToFilename + ".txt");

		try {
			FileOutputStream fos = new FileOutputStream(toF);
			PrintWriter pw = new PrintWriter(fos);

			for (double c : currentColumn) {
				pw.println(c);
			}

			pw.close();

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}

	}

}
