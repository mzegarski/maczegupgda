package theGame;

public class Card {

	private String[] rank = { "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen",
			"King", "Ace" };

	private String[] color = { "Clubs", "Diamonds", "Hearts", "Spades" };

	public Card() {

	}

	public Card(String[] rank, String[] color) {

		this.rank = rank;
		this.color = color;
	}

	public String[] getRank() {
		return rank;
	}

	public void setRank(String[] rank) {
		this.rank = rank;
	}

	public String[] getColor() {
		return color;
	}

	public void setColor(String[] color) {
		this.color = color;
	}

}
