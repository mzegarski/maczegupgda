package wielokrotnosci;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		// Metoda zwracające listę wszystkich wielokrotności a, mniejszych od b.

		for (Integer i : countMultiplesUntillLimit(3, 100)) {
			System.out.println(i + " ");

		}

	}

	public static ArrayList<Integer> countMultiplesUntillLimit(int countFrom, int limit) {

		ArrayList<Integer> multipleFromNumber = new ArrayList<Integer>();

		int multiple = countFrom;

		while ((multiple + countFrom) <= limit) {
			multiple += countFrom;
			multipleFromNumber.add(multiple);
		}

		return multipleFromNumber;
	}

}
