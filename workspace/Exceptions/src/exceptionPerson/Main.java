package exceptionPerson;

public class Main {

	public static void main(String[] args) throws Exception  {
		
		
		People p1 = new People(5);
		
		p1.addPerson("Maciej", "Zegarski", 30, "Black", "Blue", 44.5);
		p1.addPerson("Maciej", "Zegarski", 31, "Black", "Blue", 44.5);
		p1.addPerson("Maciej", "Zegarski", 32, "Black", "Blue", 44.5);
		
		
			for (Person person : p1.getPeopleList()) {
				System.out.println(person);
			}
		

	}

}
