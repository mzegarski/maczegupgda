package exceptionPerson;

import java.util.ArrayList;
import java.util.List;

public class People {

	private final String[] allowedEyes = { "Green", "Brown", "Blue", "Black" };
	private final String[] allowedHair = { "Blond", "Brown", "Black", "Red" };

	private List<Person> peopleList = new ArrayList<>();

	
	


	
	
	public List<Person> getPeopleList() {
		return peopleList;
	}

	int max = 5;

	public People(int max) {
		this.max = max;
	}

	private boolean isFull() {
		return (peopleList.size() >= max);
	}

	public void addPerson(Person person) {
		if (!isFull()) {
			peopleList.add(person);
		}
	}

	public void addPerson(String name, String secondName, int age, String hair, String eyes, double shoe)
			throws BadHairException, BadEyesException, BadShoeException, WrongAgeException
			  {

		if (checkIfContains(hair, allowedHair) == false)
			throw new BadHairException();

		if (checkIfContains(eyes, allowedEyes) == false)
			throw new BadEyesException();

		if (shoe % 0.5 != 0)
			throw new BadShoeException();

		if (!isFull())
		peopleList.add(new Person(name, secondName, age, hair, eyes, shoe));

	}

	private boolean checkIfContains(String something, String[] somewhere) {
		for (int i = 0; i < somewhere.length; i++) {
			if (something.contains(somewhere[i]))
				return true;

		}
		return false;
	}

}
