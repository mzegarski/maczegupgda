package exceptionPerson;

public class Person {

	private String name, secondName, hair, eyes;

	private int age;

	private double shoe;

	public Person(String name, String secondName, int age, String hair, String eyes, double shoe)
			throws WrongAgeException {
		super();
		this.name = name;
		this.secondName = secondName;
		this.hair = hair;
		this.eyes = eyes;
		this.shoe = shoe;
		setAge(age);
	}

	public String toString() {

		return name + " " + secondName + " [Wiek: " + age + "]" + "[W�osy: " + hair + "][Oczy: " + eyes
				+ "][Rozmiar buta: " + shoe + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getHair() {
		return hair;
	}

	public void setHair(String hair) {
		this.hair = hair;
	}

	public String getEyes() {
		return eyes;
	}

	public void setEyes(String eyes) {
		this.eyes = eyes;
	}

	public int getAge() {

		return age;
	}

	public void setAge(int age) throws WrongAgeException {
		if (age >= 130)
			throw new WrongAgeException("[ERROR] Wiek musi wynosi� do 130 lat.");
		else if (age <= 0)
			throw new WrongAgeException("[ERROR] Wiek musi wynosi� wi�cej ni� 0 lat.");

		this.age = age;
	}

	public double getShoe() {
		return shoe;
	}

	public void setShoe(double shoe) {
		this.shoe = shoe;
	}

}
