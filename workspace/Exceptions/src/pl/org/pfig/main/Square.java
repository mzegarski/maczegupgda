package pl.org.pfig.main;

public class Square {
	
	
	
	public static double square (int n) throws IllegalArgumentException
	{
		if(n < 0) {
			throw new IllegalArgumentException("bad value");
		}
		return Math.sqrt(n);
	}

}
