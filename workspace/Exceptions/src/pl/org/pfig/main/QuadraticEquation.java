package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class QuadraticEquation {

	private int a, b, c;

	public QuadraticEquation(int a, int b, int c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;

		if (a == 0 && b == 0 && c == 0) {
			getAllNumbers();
		}

	}

	public QuadraticEquation() {
		super();
		getAllNumbers();

	}

	private void getAllNumbers() {
		System.out.println("Wszystkie parametry musz� by� liczbami ca�kowitymi!");
		System.out.println("Podaj parametr a:");
		this.a = getNumber();
		System.out.println("Podaj parametr b:");
		this.b = getNumber();
		System.out.println("Podaj parametr c:");
		this.c = getNumber();

	}

	public int getNumber() throws InputMismatchException {
		Scanner scan = new Scanner(System.in);
		int i = 0;

		try {
			i = scan.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("To nie jest liczba ca�kowita!");
		} catch (NoSuchElementException e) {
			System.out.println("Nic nie zosta�o podane!");
		}

		return i;

	}

	public double[] solve() {

		double[] xOneAndTwo = new double[2];

		if (delta() < 0)
			throw new ArithmeticException("Delta jest mniejsza od zera!");

		if (a == 0) {
			xOneAndTwo[0] = c / b;
			xOneAndTwo[1] = c / b;
			return xOneAndTwo;
		}

		xOneAndTwo[0] = X1();
		xOneAndTwo[1] = X2();

		return xOneAndTwo;

	}

	public double delta() {
		return b * b - (4 * a * c);
	}

	public double X1() {

		return (-1 * b + Math.sqrt(delta())) / (2 * a);
	}

	public double X2() {

		return (-1 * b - Math.sqrt(delta())) / (2 * a);
	}

}
