package pl.org.pfig.main;

public class Division {

	public static double divide(int a, int b) {

		exceptionIfEqualZero((double) b);
		return (a / b);
	}

	public static double divide(double a, double b) {

		exceptionIfEqualZero(b);
		return (a / b);
	}

	private static void exceptionIfEqualZero(double x) throws IllegalArgumentException {
		if (x == 0) {
			throw new IllegalArgumentException("Can't divide by 0!");
		}
	}
}
