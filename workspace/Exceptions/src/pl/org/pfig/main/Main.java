package pl.org.pfig.main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		exceptionQuadraticEquation();

		// exceptionReadNumber();

		// exceptionDivision();

		// exceptionSquare();

		// exceptionString();

		// exceptionArray();

		// exceptionInty();

	}

	private static void exceptionQuadraticEquation() {
		// QuadraticEquation qe = new QuadraticEquation(2, 2, -12); // wynik 2 i
		// 3
		// QuadraticEquation qe = new QuadraticEquation(2, 2, 12); //delta
		// mniejsza niz 0
		// QuadraticEquation qe = new QuadraticEquation();
		QuadraticEquation qe = new QuadraticEquation(0, 5, 16);

		double[] wynik = qe.solve();

		for (double d : wynik) {
			System.out.println(d);
		}
	}

	private static void exceptionReadNumber() {
		ReadNumbers rn = new ReadNumbers();
		System.out.println(rn.readDouble());
		// System.out.println(rn.readInt());
		// System.out.println(rn.readString());
	}

	private static void exceptionDivision() {
		int a = 5;
		int b = 1;
		double c = 5;
		double d = 1;

		try {
			System.out.println(Division.divide(c, d));
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void exceptionSquare() {
		int n = 4;

		try {

			System.out.println(Square.square(n));

		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void exceptionString() {

		ExceptionSimpleClass esc = new ExceptionSimpleClass();
		try {
			esc.exceptionExample("MACIEJ");
		} catch (MaciejException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void exceptionArray() {
		int a = 5;

		int[] array = new int[a];
		Scanner scn = new Scanner(System.in);

		for (int i = 0; i < array.length; i++) {

			System.out.println("Podaj liczb� numer " + (i + 1));
			array[i] = scn.nextInt();
		}
		scn.close();

		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + "; ");
		}
		System.out.println();

		try {
			System.out.println(array[a + 1]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Brak elementu pod indeksem: " + e.getMessage());
		}
	}

	private static void exceptionInty() {

		ExceptionSimpleClass esc = new ExceptionSimpleClass();

		try {

			esc.make(55);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.out.println("[ERROR] " + e.getMessage());
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("[END]");

		}
	}

}
