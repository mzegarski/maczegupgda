package pl.org.pfig.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	public double readDouble() throws InputMismatchException {
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj liczbe typu double: ");
		double d;

		try {
			d = scan.nextDouble();
		} catch (InputMismatchException e) {
			d = 0.0;
		}
		scan.close();

		return d;

	}

	public int readInt() throws InputMismatchException {
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj liczbe typu int: ");
		int i;

		try {
			i = scan.nextInt();
		} catch (InputMismatchException e) {
			i = 0;
		}
		scan.close();

		return i;

	}

	public String readString() throws InputMismatchException {
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj liczbe typu String: ");
		String s;

		try {
			s = scan.nextLine();
		} catch (InputMismatchException e) {
			s = "";
		}
		scan.close();

		return s;

	}
}
