package repository;

public class RepoName {

	private String name;
	private String secondName;

	public RepoName(String name, String secondName) {
		super();
		this.name = name;
		this.secondName = secondName;
	}

	public static String getRepoName(String name, String secondName) {
		int i = 3;

		if (name.toLowerCase().charAt(2) == secondName.toLowerCase().charAt(0))
			i++;
		return name.toUpperCase().charAt(0) + name.toLowerCase().substring(1, i) + secondName.toUpperCase().charAt(0)
				+ secondName.toLowerCase().substring(1, i) + "UPGda";
	}

	public String getRepoName() {
		return getRepoName(name, secondName);
	}

	public String getName() {
		return name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

}
