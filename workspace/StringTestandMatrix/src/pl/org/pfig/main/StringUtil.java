//Napiszmy klas� StringUtil, kt�ra b�dzie posiada�a konstruktor sparametryzowany:
//
//StringUtil(String);
//
//kt�rego zadaniem b�dzie przypisanie stringa do pola prywatnego. Nast�pnie dodajmy metody:
//
//public StringUtil prepend(String str); // dodkleja nowy ci�g przed aktualnym
//public StringUtil append(String str); // dodkleja nowy ci�g za aktualnym
//public StringUtil letterSpacing(); // rozdziela string spacjami
//public StringUtil reverse(); // ustawia string 'od ty�u'
//public StringUtil getAlphabet(); // ustawia string z alfabetem 'abcdefgh...z';
//public StringUtil getFirstLetter(); // ustawia pierwsz� liter� zamiast ca�ego ci�gu
//public StringUtil limit(int n); // ucina string we wskazanym miejscu
//public StringUtil insertAt(String string, int n); // umieszcza zadany string pod wybrana pozycja
//public StringUtil resetText(); // ustawia pusty ciag znakow
//public StringUtil swapLetters(); // ustawia ci�g znak�w z zamienion� pierwsz� i ostatni� liter�.
//public StringUtil createSentence(); // ustawia ci�g znak�w z pierwsz� wielk� liter� i z kropk� na ko�cu. Nale�y sprawdzi�, czy kropka na ko�cu nie wyst�powa�a wcze�niej.
//public StringUtil cut(int from, int to); // ustawia obci�ty string od..do (bez u�ycia metody substring z klasy String!)
//public StringUtil pokemon(); // ustawia  cIaG zNaKoW pIsMeM pOkEmOn ;)
//public StringUtil print(); // drukuje ci�g do konsoli
//
//
//public StringUtil getRandomHash(int n); // przypisuje do zmiennej n losowych znakow z zakresu [0-9a-f];
//
//Oraz metody dodatkowe, nie operuj�ce na wczytanym stringu:
//
//public static String join(String[] arrayOfStrings, String delimeter); // zwraca String rozdzielony delimeterem.
//
//Metody powinny zwraca� instancj� sam� w sobie, tak aby�my mogli u�y� chaining.

package pl.org.pfig.main;

import java.util.Random;

public class StringUtil {

	private String str;

	public StringUtil(String str) {
		this.str = str;
	}

	public StringUtil print() {
		System.out.println(str);
		return this;
	}

	public StringUtil prepend(String arg) {
		str = arg + str;
		return this;
	}

	public StringUtil append(String arg) {
		str = str + arg;
		return this;
	}

	public StringUtil letterSpacing() {
		String arrayStr[] = this.str.split("");
		this.str = "";
		for (String s : arrayStr) {
			this.str += s + " ";
		}
		return this;

	}

	public StringUtil getAlphabet() {
		this.str = "";
		for (char a = 'a'; a <= 'z'; a++) {
			this.str += a;
		}
		return this;

	}

	public StringUtil getFirstLetter() {
		this.str = str.charAt(0) + "";
		return this;

	}

	public StringUtil limit(int n) {
		this.str = str.substring(0, n);
		return this;

	}

	public StringUtil insertAt(String string, int n) {
		this.str = str.substring(0, n) + string + str.substring(n, str.length());
		return this;
	}

	public StringUtil resetText() {
		this.str = "";
		return this;
	}

	public StringUtil swapLetters() {
		this.str = str.charAt(str.length() - 1) + str.substring(1, str.length() - 1) + str.charAt(0);
		return this;
	}

	public StringUtil createSentence() {

		char first = str.charAt(0);

		if (first >= 'a' && first <= 'z')
			first -= ' '; // ASCII 32 - space

		if (str.charAt(str.length() - 1) == '.') {
			this.str = first + str.substring(1, str.length());
		}

		else {
			this.str = first + str.substring(1, str.length()) + '.';
		}

		return this;

	}

	public StringUtil cut(int from, int to) {
		String arrayStr[] = this.str.split("");
		this.str = "";

		for (; from < to; from++) {
			this.str += arrayStr[from];
		}

		return this;

	}

	public StringUtil pokemon() {

		String arrayStr[] = this.str.split("");

		this.str = "";

		for (int i = 0; i < arrayStr.length; i++) {
			if (i % 2 == 0) {
				this.str += "" + arrayStr[i].toLowerCase();
			} else {
				this.str += "" + arrayStr[i].toUpperCase();
			}
		}

		return this;

	}

	public StringUtil getRandomHash(int n) {

		this.str = "";
		Random r = new Random();

		for (int i = 0; i < n; i++) {
			if (r.nextInt(2) == 1) {
				this.str += r.nextInt(9);
			} else {// 97 = a 102 = f
				this.str += (char) (r.nextInt(103 - 97) + 97);
			}

		}
		return this;

	}
	
	public static String join(String[] arrayOfStrings, String delimeter)
	{
		String s = "";
		
		for (int i = 0; i< (2*(arrayOfStrings.length))-1; i++)
		{
			if (i%2 == 1){
				s += "" + delimeter;
			}
			else{
				s+= "" + arrayOfStrings[i/2];
			}
			
		}
		
		return s;
		
	}
	
	

}
