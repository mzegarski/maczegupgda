package pl.org.pfig.main;

import java.util.Random;

public class Main {
	public static void main(String[] args) {
		
		Integer[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }  };
		Integer[][] m2 = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		
		Matrix.print(m);
		System.out.println();
		Matrix.print(Matrix.identityMatrix(m));
		
		System.out.println();
		
		Matrix.print(Matrix.countInMatrix(m));
		
		System.out.println(Matrix.areMatrixesEqual(m, m2));
		
		Matrix.print(Matrix.add(m, m2));
		
		
		
		//delimeter();

		//stringUntil();

		//mainString();

		//htmlExercise();

	}

	private static void htmlExercise() {
		HTMLExercise he = new HTMLExercise("To jest m�j tekst");
		he.print().strong().print().p().print();
	}

	private static void delimeter() {
		String [] array = {"1", "2", "3", "4", "5"};
		String delimeter = "+";
		System.out.println(StringUtil.join(array, delimeter));
	}

	private static void stringUntil() {
		StringUtil su = new StringUtil("test raz dwa trzy");

		su.getAlphabet().print().limit(5).print().insertAt("dupa", 2).print().getAlphabet().swapLetters().print()
				.createSentence().print().createSentence().print().cut(2, 4).print().getAlphabet().pokemon().print();

		StringUtil su2 = new StringUtil("i wanna be pokemon master!");

		su2.pokemon().print().getRandomHash(10).print();
	}

	private static void mainString() {
		String myStr = "  przyk�adowy ci�g znak�w  ";

		System.out.println(myStr);

		if (myStr.equals("  przyk�adowy ci�g znak�w  ")) {
			System.out.println("Ci�gi s� takie same.");
		}

		if (myStr.equalsIgnoreCase("  PRZYK�ADOWY ci�g znaK�W  ")) {
			System.out.println("Ci�gi znak�w s� takie same, bez badania wielko�ci liter.");
		}

		System.out.println("D�ugo�� myStr to: " + myStr.length());

		System.out.println(myStr.substring(14));

		String otherStr = "MACIEJ";

		System.out.println(otherStr.substring(1, 5));

		System.out.println(otherStr.substring(1, otherStr.length() - 1));

		System.out.println(myStr.trim());

		System.out.println(myStr.charAt(2) + "" + myStr.charAt(3));

		String alphabet = "";
		for (char c = 97; c <= 122; c++) { // for (char c ='a'; c<=122; c++)
			alphabet += c;
			// alphabet += c + " ";
		}

		System.out.println(alphabet);

		System.out.println(myStr.replace("ci�g", "�a�cuch"));

		System.out.println(myStr.concat(otherStr));

		System.out.println(myStr);

		if (myStr.contains("k�ad"))
			System.out.println("Znaki \"k�ad\" zawieraj� si� w " + myStr);

		if (alphabet.startsWith("a"))
			System.out.println("Alfabet zaczyna si� od a");

		if (alphabet.endsWith("z"))
			System.out.println("Alfabet ko�czy si� na z");

		System.out.println(myStr.indexOf("a"));
		System.out.println(myStr.lastIndexOf("a"));

		String simpleStr = "maciej poszed� do lasu";
		String[] arrOfStr = simpleStr.split(" ");
		for (String s : arrOfStr)
			System.out.println("\t" + s);

		// Napisz funkcj�, w kt�rej dla zadanego �a�cucha znak�w, wszystkie
		// znaki - takie same jak
		// pierwsza litera ci�gu znak�w zostan� zamienione na znak '_',
		// wyj�tkiem jednak jest pierwszy
		// znak. Dla przyk�adu:
		// Wej�cie: oksymoron
		// Wyj�cie: oksym_r_n

		String cw1Str = "abrakadabra";
		System.out.println(cw1Str);
		System.out.println(cw1Str.charAt(0) + cw1Str.substring(1).replace(cw1Str.charAt(0) + "", "_"));

		// Napisz funkcj�, kt�ra przyjmie dwa argumenty: liczb� znak�w oraz
		// zdanie. Funkcja powinna zwr�ci� list� ze s�owami, kt�re s� d�u�sze
		// ni� zadana liczba w pierwszym argumencie. Przyk�ad:
		//
		// Wej�cie: 3, "kiedy� si� wybior� do lasu"
		//
		// Wyj�cie: ['kiedy�', 'wybior�', 'lasu']
		//
		// public static String[] checkWordsLength(int num, String sentence) {
		//
		// }

		System.out.println();

		int n = 3;
		String cw2Str = "kiedy� si� wybior� do lasu";
		checkWordsLength(n, cw2Str);

		System.out.println(myStr.toUpperCase());
		System.out.println(myStr.toLowerCase());

		Random r = new Random();
		System.out.println(r.nextInt()); // liczba losowa z zakresu -int +int
		System.out.println(r.nextInt(15)); // liczba losowa z zakresu <0, liczba

		// <10, 25) 25-10 = 15
		int a = 10;
		int b = 25;
		System.out.println(a + r.nextInt(b - a));

		char c = (char) (97 + r.nextInt(26));
		System.out.println(c);
	}

	public static void checkWordsLength(int numOfLetters, String anyString) {
		String[] arrOfCw2Str = anyString.split(" ");

		// // standardowy for z licznikiem
		// for (int i = 0; i < arrOfCw2Str.length; i++)
		// {
		// if (arrOfCw2Str[i].length() > numOfLetters) {
		// System.out.println(arrOfCw2Str[i]);
		// }
		// }

		// foreach
		for (String s : arrOfCw2Str) {

			if (s.length() > numOfLetters) {
				System.out.println(s);
			}
		}
	}

}
