//Utw�rz klas� Matrix
//
//1. Napisz funkcj� drukuj�c� tablic� dwuwymiarow�, tak aby zosta�a
//wydrukowana jako macierz.
//Wej�cie:
//int[][] m = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
//Wyj�cie:
//1 2 3
//4 5 6
//7 8 9
//
//2. Napisz funkcj�, kt�ra przyjmie jako parametr tablic� dwuwymiarow� i
//przypisze 1 jako jej warto�ci
//w ka�dym z p�l - tak� konstrukcj� nazywamy macierz� jednostkow�. Funkcja
//powinna zwraca�
//uzupe�nion� tablic� dwuwymiarow�.
//
//3. Napisz funkcj�, kt�ra przyjmie jako parametr tablic� dwuwymiarow� i
//przypisze kolejne liczby od 1
//jako jej warto�ci. Funkcja powinna zwraca� uzupe�nion� tablic�
//dwuwymiarow�.
//
//4. Napisz funkcj� sprawdzaj�c� czy dwie przekazane jako argumenty tablice
//dwuwymiarowe maj� takie wymiary (tj. zar�wno w jednej jak i w drugiej
//tablicy ilo�� wierszy i kolumn jest taka sama).
//
//5. Napisz funkcj�, kt�ra doda do siebie dwie macierze. Funkcja powinna
//zwraca� sum� macierzy (jako macierz). Zwr�� uwag� na to, �e obie macierze
//musz� mie� takie same wymiary.
//
//6. Napisz funkcj�, kt�ra odejmie od siebie dwie macierze. Funkcja powinna
//zwraca� r�nic� macierzy
//(jako macierz). Zwr�� uwag� na to, �e obie macierze musz� mie� takie same
//wymiary.
//
//7. Napisz funkcj�, kt�ra przemno�y macierz przez liczb� (ka�da z kom�rek
//musi zosta� pomno�ona przez t� liczb�).
//
//8. Napisz funkcj�, kt�ra pozowli na realizacj� tranpozycji macierzy
//(tranpozycja, w skr�cie zamiana wierszy z kolumnami /tak�e je�li chodzi o
//wymiary/).
//
//9. Napisz funkcj� sprawdzaj�c�, czy macierz jest symetryczna ( tj. A(T) =
//A ).

package pl.org.pfig.main;

public class Matrix {

	public static void print(Integer[][] matrix) {

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static Integer[][] identityMatrix(Integer[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = 1;
			}

		}

		return matrix;

	}

	public static Integer[][] countInMatrix(Integer[][] matrix) {
		int n = 1;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				matrix[i][j] = n++;
			}

		}

		return matrix;

	}

	public static boolean areMatrixesEqual(Integer[][] matrix_one, Integer[][] matrix_two) {

		if (matrix_one.length == matrix_two.length) {
			for (int i = 0; i < matrix_one.length; i++) {
				if (!(matrix_one[i].length == matrix_two[i].length)) {
					return false;
				}
			}
		} else
			return false;

		return true;

	}

	public static Integer[][] add(Integer[][] matrix_one, Integer[][] matrix_two) {

		if (areMatrixesEqual(matrix_one, matrix_two) == false)
			return null;
		System.out.println();
		Matrix.print(matrix_one);
		System.out.println("+");
		Matrix.print(matrix_two);
		System.out.println("=");

		for (int i = 0; i < matrix_one.length; i++) {
			for (int j = 0; j < matrix_one[i].length; j++) {
				matrix_one[i][j] += matrix_two[i][j];
			}
		}

		return matrix_one;

	}

}
