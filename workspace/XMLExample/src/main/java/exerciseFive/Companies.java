package exerciseFive;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Companies {

	static final String FILE_NAME = "src/main/resources/companies.xml";

	static Document doc;

	public static void main(String[] args) {

		buildCompanyXMLFile();

	}

	public static void buildCompanyXMLFile() throws TransformerFactoryConfigurationError {
		try {
			makeDocumentBuilder();

			Element rootElement = createRoot("root");

			Element company = createParentElement(rootElement, "Testowa");
			createElement("Starts", "2008", company);
			createElement("Employess", "345", company);
			createElement("Vat", "23", company);

			Element company2 = createParentElement(rootElement, "Testowa 2");
			createElement("Starts", "1979", company2);
			createElement("Employess", "34345", company2);
			createElement("Vat", "40", company2);

			Element company3 = createParentElement(rootElement, "Testowa 3");
			createElement("Starts", "1999", company3);
			createElement("Employess", "5", company3);
			createElement("Vat", "8", company3);

			saveToXMLFile(FILE_NAME);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void saveToXMLFile(String file)
			throws TransformerFactoryConfigurationError, TransformerConfigurationException, TransformerException {
		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(file));
		transformer.transform(source, result);
		// // Output to console for testing
		// StreamResult consoleResult = new StreamResult(System.out);
		// transformer.transform(source, consoleResult);
	}

	public static Element createRoot(String root) {
		Element rootElement = doc.createElement(root);
		doc.appendChild(rootElement);
		return rootElement;
	}

	public static void makeDocumentBuilder() throws ParserConfigurationException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		doc = dBuilder.newDocument();
	}

	public static Element createParentElement(Element rootElement, String companyName) {
		Element company = doc.createElement("Company");
		rootElement.appendChild(company);
		Attr compName = doc.createAttribute("name");
		compName.setValue(companyName);
		company.setAttributeNode(compName);
		return company;
	}

	public static void createElement(String textName, String value, Element company) {
		Element name = doc.createElement(textName);
		company.appendChild(name);
		name.appendChild(doc.createTextNode(value));
	}

}
