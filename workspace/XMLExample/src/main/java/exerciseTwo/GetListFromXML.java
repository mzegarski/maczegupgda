package exerciseTwo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GetListFromXML {

	static final String FILE_NAME = "src/main/resources/staff.xml";

	public static void main(String[] args) {

		for (String string : getStaffToList()) {
			System.out.println(string);
		}
		
		
	}

	public static ArrayList<String> getStaffToList() {

		ArrayList<String> staffList = new ArrayList<String>();

		try {

			NodeList nList = getDataFromXMLByTagName(FILE_NAME, "staff");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					staffList.add(eElement.getElementsByTagName("firstname").item(0).getTextContent() 
							+ " " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return staffList;
	}

	public static NodeList getDataFromXMLByTagName(String file, String tagName)
			throws ParserConfigurationException, SAXException, IOException {
		File inputFile = new File(file);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName(tagName);
		return nList;
	}

}
