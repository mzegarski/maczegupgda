package exerciseThreeAndFour;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class MaptoXML {

	static final String FILE_NAME = "src/main/resources/staff2.xml";

	public static void main(String[] args) {

		Map<Integer, Staff> staffMap = XMLtoMap.loadFromXMLtoMap();

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			Element rootElement = doc.createElement("root");
			doc.appendChild(rootElement);

			for (Entry<Integer, Staff> s : staffMap.entrySet()) {

				Element staff = doc.createElement("staff");
				rootElement.appendChild(staff);
				Attr id = doc.createAttribute("id");
				staff.setAttributeNode(id);
				id.setValue(s.getKey().toString());

				Element name = doc.createElement("name");
				staff.appendChild(name);
				name.appendChild(doc.createTextNode(s.getValue().getName()));

				Element surname = doc.createElement("surname");
				staff.appendChild(surname);
				surname.appendChild(doc.createTextNode(s.getValue().getSurname()));

				Element salary = doc.createElement("salary");
				staff.appendChild(salary);
				salary.appendChild(doc.createTextNode("" + s.getValue().getSalary()));

			}

			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(FILE_NAME));
			transformer.transform(source, result);
//			// Output to console for testing
//			StreamResult consoleResult = new StreamResult(System.out);
//			transformer.transform(source, consoleResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
