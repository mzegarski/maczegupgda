package exerciseThreeAndFour;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLtoMap {

	static final String FILE_NAME = "src/main/resources/staff.xml";

	public static void main(String[] args) {

		Map<Integer, Staff> staffMap = loadFromXMLtoMap();

		System.out.println(staffMap.toString());

		int idMin = 0;
		int idMax = 0;

		for (Entry<Integer, Staff> s : staffMap.entrySet()) {

			if (idMin == 0)
				idMin = s.getKey();
			if (idMax == 0)
				idMax = s.getKey();

			if (idMin >= s.getKey())
				idMin = s.getKey();
			if (idMax <= s.getKey())
				idMax = s.getKey();

		}

		System.out.println("Min: " + idMin + " Max: " + idMax);

	}

	public static Map<Integer, Staff> loadFromXMLtoMap() {

		Map<Integer, Staff> staffMap = new HashMap<Integer, Staff>();

		try {

			NodeList nodeList = loadDataFromXML(FILE_NAME, "staff");

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					staffMap.put(Integer.parseInt(eElement.getAttribute("id")), new exerciseThreeAndFour.Staff(
							Integer.parseInt(eElement.getAttribute("id")),
							eElement.getElementsByTagName("firstname").item(0).getTextContent(),
							eElement.getElementsByTagName("lastname").item(0).getTextContent(),
							Integer.parseInt(eElement.getElementsByTagName("salary").item(0).getTextContent())));
				}
			}

			return staffMap;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return staffMap;

	}

	public static NodeList loadDataFromXML(String file, String tagName)
			throws ParserConfigurationException, SAXException, IOException {
		File inputFile = new File(file);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName(tagName);
		return nList;
	}

}
