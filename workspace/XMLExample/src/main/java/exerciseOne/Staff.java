package exerciseOne;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Staff {

	static final String FILE_NAME = "src/main/resources/staff.xml";

	public static void main(String[] args) {
		System.out.println(getAvgSalary());
	}

	public static double getAvgSalary() {

		try {

			Document doc = loadXMLFileFrom(FILE_NAME);

			NodeList nodeList = doc.getElementsByTagName("staff");

			double salarySum = 0;
			int countStaff = 0;

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) nNode;

					salarySum += Double.parseDouble(getElementValue(element, "salary"));
					countStaff++;
				}
			}

			return (salarySum / countStaff);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0.0;
	}

	public static String getElementValue(Element element, String tag) {
		return element.getElementsByTagName(tag).item(0).getTextContent();
	}

	public static Document loadXMLFileFrom(String file) throws ParserConfigurationException, SAXException, IOException {
		File inputFile = new File(file);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
		return doc;
	}

}
