package matrix;

public class MatrixMain {

	public static void main(String[] args) {

		Matrix m = new Matrix();

		int a = 5;
		int b = 5;

		int[][] matrix = new int[a][b];

		m.print2DArray(m.identityMatrix(matrix));
		
		m.print2DArray(m.indexedMatrix(matrix));

	}

}
