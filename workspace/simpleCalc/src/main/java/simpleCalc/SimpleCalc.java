package simpleCalc;

public class SimpleCalc {

	public int add(int a, int b) {
		return a + b;
	}

	public double divide(double a, double b) {
		return a / b;
	}

	public int exThrow() throws Exception {
		throw new Exception("Wyjątek");

	}

	// przypadki testowe
	// 1. czy wynik nie wyjdzie poza zakres int
	// 2. a+b, -a+b, a-b, -a-b

	// Przypadek testowy
	// 1. Dane wejściowe
	// 2. Wstępne warunki wykonania - nie ma w tym przypadku
	// 3. Kroki wykonania - glownie dla testow manualnych
	// a) Podanie pierwszej liczby
	// b) Podanie drugiej liczby
	// c) Oczekiwany rezultat

}
