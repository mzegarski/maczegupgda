package testMatrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class IsEqualMethodDimension {

	private Matrix mt;

	@Before
	public void doStuff() {

		mt = new Matrix();
	}

	@Test
	public void twoEmptySameSizeArraysGivenExpectedToReturnTrue() {

		int[][] matrixOne = new int[3][3];
		int[][] matrixTwo = new int[3][3];

		assertTrue("False", mt.isEqualDimension(matrixOne, matrixTwo));

	}

	@Test
	public void twoFilledSameSizeArraysGivenExpectedToReturnTrue() {

		int[][] matrixOne = new int[3][3];
		int[][] matrixTwo = new int[3][3];

		assertTrue("False", mt.isEqualDimension(matrixOne, matrixTwo));

	}

	@Test
	public void firstArraySmallerSecondLargerGivenExpectedtoReturnFalse() {

		int[][] matrixOne = new int[3][3];
		int[][] matrixTwo = new int[5][5];

		assertFalse("True", mt.isEqualDimension(matrixOne, matrixTwo));

	}

	@Test
	public void firstArrayLargerSecondSmallerGivenExpectedToReturnFalse() {

		int[][] matrixOne = new int[5][5];
		int[][] matrixTwo = new int[3][3];

		assertFalse("True", mt.isEqualDimension(matrixOne, matrixTwo));

	}

}
