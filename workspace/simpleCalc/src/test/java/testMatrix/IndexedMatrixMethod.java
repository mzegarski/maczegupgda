package testMatrix;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class IndexedMatrixMethod {

	private Matrix mt;

	@Before
	public void doStuff() {

		mt = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenIndexedMatrixExpected() {

		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		assertArrayEquals(expected, mt.indexedMatrix(actual));

	}

	@Test
	public void whenNotEmptyArrayGivenIndexedMatrixExpected() {

		int[][] actual = { { 4, 5, 6 }, { 7, 8, 9 }, { 1, 2, 3 } };
		int[][] expected = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		assertArrayEquals(expected, mt.indexedMatrix(actual));

	}

	@Test
	public void whenEmptyLongerThanWiderArrayGivenIndexedMatrixExpected() {

		int[][] actual = new int[3][5];
		int[][] expected = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };

		assertArrayEquals(expected, mt.indexedMatrix(actual));

	}

	public void whenEmptyWiderThanLonderArrayGivenIndexedMatrixExpected() {

		int[][] actual = new int[5][3];
		int[][] expected = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 }, { 13, 14, 15 } };

		assertArrayEquals(expected, mt.indexedMatrix(actual));

	}

}
