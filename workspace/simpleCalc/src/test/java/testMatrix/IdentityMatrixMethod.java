package testMatrix;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class IdentityMatrixMethod {

	private Matrix mt;

	@Before
	public void doStuff() {

		mt = new Matrix();
	}

	@Test
	public void whenEmptyArrayGivenIdentityMatrixExpected() {

		int[][] actual = new int[3][3];
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		assertArrayEquals(expected, mt.identityMatrix(actual));

	}

	@Test
	public void whenNOTEmptyArrayGivenIdentityMatrixExpected() {

		int[][] actual = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int[][] expected = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		assertArrayEquals(expected, mt.identityMatrix(actual));

	}

}
