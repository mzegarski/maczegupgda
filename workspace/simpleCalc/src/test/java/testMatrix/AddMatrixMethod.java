package testMatrix;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class AddMatrixMethod {

	private Matrix mt;

	@Before
	public void doStuff() {

		mt = new Matrix();
	}

	@Test
	public void TwoSameSizeArraysGivenExpectedToSumProperly() {

		int[][] matrixOne = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int[][] matrixTwo = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		int[][] expected = { { 2, 4, 6 }, { 8, 10, 12 }, { 14, 16, 18 } };

		assertArrayEquals(expected, mt.addMatrix(matrixOne, matrixTwo));

	}

	@Test(expected = Exception.class)
	public void TwoDifferentSizeArraysGivenExpectedToThrowError() {

		int[][] matrixOne = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9, 10 } };
		int[][] matrixTwo = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		mt.addMatrix(matrixOne, matrixTwo);

	}

	@Test
	public void SecondArrayIsFilledFirstIsEmptyBothSameSizeExpectedToSumProperlyAndReturnValuesFromNotEmptyArray() {

		int[][] matrixOne = new int[3][3];
		int[][] matrixTwo = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		assertArrayEquals(matrixTwo, mt.addMatrix(matrixOne, matrixTwo));

	}

	@Test
	public void FirstArrayIsFilledSecondIsEmptyBothSameSizeExpectedToSumProperlyAndReturnValuesFromNotEmptyArray() {

		int[][] matrixOne = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		int[][] matrixTwo = new int[3][3];

		assertArrayEquals(matrixOne, mt.addMatrix(matrixOne, matrixTwo));

	}

}
