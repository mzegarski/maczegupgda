package testSimpleCalc;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import simpleCalc.SimpleCalc;

public class SimpleCalcDivideMethodTest {

	private SimpleCalc sc;

	@Before
	public void doStuff() {

		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreDivided() {

		double a = 3.12, b = 6.15;

		assertEquals(0.50731707, sc.divide(a, b), 0.0001);

	}

	@Test
	public void whenFirstNumberIsNegtiveAndSecondIsPositive() {

		double a = -3.12, b = 6.15;

		assertEquals(-0.50731707, sc.divide(a, b), 0.0001);

	}

	@Test
	public void whenFirstNumberIsPositiveAndSecondIsNegative() {

		double a = 3.12, b = -6.15;

		assertEquals(-0.50731707, sc.divide(a, b), 0.0001);

	}

	@Test
	public void whenBothNumbersAreNegative() {

		double a = -3.12, b = -6.15;

		assertEquals(0.50731707, sc.divide(a, b), 0.0001);

	}

}
