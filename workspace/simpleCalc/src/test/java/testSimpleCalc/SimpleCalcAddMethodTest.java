package testSimpleCalc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import simpleCalc.SimpleCalc;

public class SimpleCalcAddMethodTest {

	// przypadki testowe
	// 1. czy wynik nie wyjdzie poza zakres int
	// 2. a+b, -a+b, a-b, -a-b

	private SimpleCalc sc;

	@Before
	public void doStuff() {

		sc = new SimpleCalc();
	}

	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumberAreSum() {

		int a = 3, b = 6;

		assertEquals(9, sc.add(a, b));

	}

	@Test
	public void whenFirstNumberIsNegativeAndSecondNumberIsPositive() {
		int a = -3, b = 6;

		assertEquals(3, sc.add(a, b));

	}

	@Test
	public void whenFirstNumberIsPositiveAndSecondNumberIsNegative() {
		int a = 12, b = -6;

		assertEquals(6, sc.add(a, b));

	}

	@Test
	public void whenBothNumbersAreNegative() {
		int a = -5, b = -6;

		assertEquals(-11, sc.add(a, b));

	}

	@Test
	public void whenSumOfNumbersIsBiggerThanIntAllowance() {
		// -2,147,483,648 to 2,147,483,647
		int a = 2147483640, b = 5;

		assertTrue("Poza zakresem int", sc.add(a, b) > 0);

	}

	@Test(expected = Exception.class)
	public void whenExThrowMethodIsUsedExceptionIsThrown() throws Exception {

		sc.exThrow();
	}

}
