package university;

import java.io.Serializable;

public class Student implements Serializable {
	private int indexNumber;
	private String name;
	private String surname;

	public Student() {

	}

	public Student(int indexNumber, String name, String surname) {
		this.indexNumber = indexNumber;
		this.name = name;
		this.surname = surname;
	}

	public int getIndexNumber() {
		return indexNumber;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public void setIndexNumber(int indexNumber) {
		this.indexNumber = indexNumber;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

}