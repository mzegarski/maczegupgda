package university;

import static xml.XmlUtilities.getInfoAbout;
import static xml.XmlUtilities.newDocument;
import static xml.XmlUtilities.parseFile;
import static xml.XmlUtilities.saveFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class XMLFile implements InterfaceFile {

	static final String FILE_NAME = "src/main/resources/xml/students.xml";

	@Override
	public void save(List<Student> studentList) {
		try {
			Document doc = newDocument();
			new XMLFile().createContent(doc, studentList);
			saveFile(doc, FILE_NAME);
		} catch (ParserConfigurationException | TransformerFactoryConfigurationError | TransformerException e) {

			e.printStackTrace();
		}

	}

	public void createContent(Document doc, List<Student> studentList) {
		Element students = doc.createElement("student");
		doc.appendChild(students);

		for (Student student : studentList) {
			Element studentElement = doc.createElement("student");
			studentElement.setAttribute("index", String.valueOf(student.getIndexNumber()));

			Element name = doc.createElement("name");
			name.appendChild(doc.createTextNode(student.getName()));
			studentElement.appendChild(name);
			students.appendChild(studentElement);

			Element surname = doc.createElement("surname");
			surname.appendChild(doc.createTextNode(student.getSurname()));
			studentElement.appendChild(surname);
			students.appendChild(studentElement);

		}

	}

	@Override
	public List<Student> load() {

		List<Student> studentsList = new ArrayList<>();

		try {
			Document doc = parseFile(FILE_NAME);
			NodeList students = doc.getElementsByTagName("student");
			for (int i = 0; i < students.getLength(); i++) {
				Node studentNode = students.item(i);
				Student s = parse(studentNode);
				studentsList.add(s);
			}

		} catch (ParserConfigurationException | SAXException | IOException e) {

			e.printStackTrace();
		}

		return studentsList;
	}
//TODO tutaj na g�rze lub na dole co� si� pierdoli
	private Student parse(Node studentNode) {

		Element s = (Element) studentNode;
		System.out.println(s.getAttribute("index") +"!!!!!!!!!!!!!!");
		
		return new Student(Integer.parseInt(s.getAttribute("index")), getInfoAbout(s, "name"),
				getInfoAbout(s, "name"));

	}

}