package university;

import java.util.List;

public interface InterfaceFile {

	void save(List<Student> studentList);

	List<Student> load();

}