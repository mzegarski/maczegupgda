package university;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements InterfaceFile {

	private String filename = "src/main/resources/university.txt";

	@Override
	public void save(List<Student> studentList) {

		try (PrintStream ps = new PrintStream(new FileOutputStream(filename, true))) {

			for (Student student : studentList) {

				ps.println(student.getIndexNumber() + "\t" + student.getName() + "\t" + student.getSurname());
			}

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public List<Student> load() {

		List<Student> sList = new LinkedList<>();

		try (Scanner scan = new Scanner(new BufferedInputStream(new FileInputStream(filename)))) {

			String currentLine = "";

			while (scan.hasNextLine()) {

				currentLine = scan.nextLine();

				String temp[] = currentLine.split("\t");

				Student tempStudent = new Student(Integer.parseInt(temp[0]), temp[1], temp[2]);

				sList.add(tempStudent);

			}
		} catch (FileNotFoundException e) {
			System.out.println("[ERROR] NO FILE FOUND");
		}

		return sList;
	}

}
