package university;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		University univ = new University();

		univ.addStudent(100100, "Adam", "Kowalski");
		univ.addStudent(100101, "Ania", "Nowak");
		univ.addStudent(100102, "Janek", "Paw�owski");
		univ.addStudent(100103, "Dobromir", "Tomaszewski");

		//InterfaceFile textFile = new TextFile();
		//InterfaceFile textFile = new BinaryFile();
		//InterfaceFile textFile = new SerializedFile();
		//InterfaceFile file = new JSONFile();
		InterfaceFile file = new XMLFile();

		file.save(univ.getStudentList());

		System.out.println();
		
		List<Student> studentsFromFile = file.load();

		University univFromFile = new University(studentsFromFile);

		univFromFile.showAll();

	}

}
