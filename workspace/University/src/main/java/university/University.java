package university;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class University {
	private Map<Integer, Student> students = new HashMap<>();

	public University() {

	}

	public University(List<Student> studentsList) {

		for (Student student : studentsList) {

			students.put(student.getIndexNumber(), student);

		}

	}

	public void addStudent(int indexNumber, String name, String surname) {
		Student student = new Student(indexNumber, name, surname);
		students.put(student.getIndexNumber(), student);
	}

	public boolean studentExists(int indexNumber) {
		// Delegacja
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		// Delegacja
		return students.get(indexNumber);
	}

	public int studentsNumber() {
		return students.size();
	}

	public void showAll() {
		for (Student s : students.values()) {
			System.out.println(s.getIndexNumber() + " : " + s.getName() + " " + s.getSurname());
		}
	}

	public List<Student> getStudentList() {

		List<Student> sList = new LinkedList<>();

		for (Student student : students.values()) {

			sList.add(student);

		}

		return sList;

		// return new LinkedList<>(students.values());
	}
}