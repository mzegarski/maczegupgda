package university;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BinaryFile implements InterfaceFile {

	private String filename = "src/main/resources/binary.txt";

	@Override
	public void save(List<Student> studentList) {
		try (DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(filename)))) {

			for (Student student : studentList) {
				out.writeInt(student.getIndexNumber());

				out.writeUTF(student.getName());

				out.writeUTF(student.getSurname());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Student> load() {

		List<Student> studentList = new ArrayList<>();

		try (DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)))) {

			while (in.available() > 0) {
				studentList.add(new Student(in.readInt(), in.readUTF(), in.readUTF()));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return studentList;
	}

}
