package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static xml.XmlUtilities.*;

public class ParseStudents {

	static final String FILE_NAME = "src/main/resources/xml/class.xml";

	public static void main(String[] args) {

		try {

			Document doc = parseFile(FILE_NAME);

			printAllStudentsInfo(doc);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static void printAllStudentsInfo(Document doc) {
		NodeList students = doc.getElementsByTagName("student");
		System.out.println("----------------------------");
		for (int i = 0; i < students.getLength(); i++) {
			printStudentInfo(students, i);
		}
	}

	static void printStudentInfo(NodeList nList, int i) {
		Node students = nList.item(i);
		System.out.println("\nCurrent Element :" + students.getNodeName());
		if (students.getNodeType() == Node.ELEMENT_NODE) {
			Element student = (Element) students;
			System.out.println("Student index: " + student.getAttribute("index"));
			System.out.println("First Name : " + getInfoAbout(student, "firstname"));
			System.out.println("Last Name : " + getInfoAbout(student, "lastname"));
			System.out.println("Nick Name : " + getInfoAbout(student, "nickname"));
			System.out.println("Marks : " + getInfoAbout(student, "marks"));
		}
	}

}
