package xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static xml.XmlUtilities.*;

import javax.xml.transform.TransformerFactoryConfigurationError;

public class CreateCarsXmlFile {

	static final String FILE_NAME = "src/main/resources/xml/cars.xml";

	Document doc;

	public static void main(String argv[]) {

		new CreateCarsXmlFile().create();
	}

	public void create() throws TransformerFactoryConfigurationError {
		try {
			doc = newDocument();
			createContent();
			saveFile(doc, FILE_NAME);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createContent() {
		Element rootElement = createRootElement("cars");
		Element supercars = addSuperCats(rootElement);
		setAttribute(doc, supercars, "company", "Ferrari");
		addCar(supercars, "formula one", "Ferrari 101");
		addCar(supercars, "sports", "Ferrari 202");
	}

	public void addCar(Element supercar, String type, String name) {
		Element carname = doc.createElement("carname");
		setAttribute(doc, supercar, "type", type);
		carname.appendChild(doc.createTextNode(name));
		supercar.appendChild(carname);
	}

	public Element addSuperCats(Element rootElement) {
		Element supercar = doc.createElement("supercars");
		rootElement.appendChild(supercar);
		return supercar;
	}

	public Element createRootElement(String name) {
		Element rootElement = doc.createElement(name);
		doc.appendChild(rootElement);
		return rootElement;
	}
}
