
package stare;

public class HelloWorld {

	public static void main (String[] args) {
		
		String text; //deklaracja zmiennej
		text = "Hello World"; //inicjalizacja zmiennej
		System.out.println(text); //wypisanie wartosci zmiennej na ekran
		
		byte b = 10;
		int i = 66;
		float f = 2.55f;
		double d = 4.22;
		char c = 'C';
		long l = 9999999L;
		boolean bool = true;
		
		System.out.println("Byte: " +b);
		System.out.println("Int: " +i);
		System.out.println("Float: " +f);
		System.out.println("Double: " +d);
		System.out.println("Char: " +c);
		System.out.println("Long: " +l);
		System.out.println("Bool: " +bool);
		
		System.out.println("Razem: "+b +i +f +d +c +l);
		
		System.out.println((char) 98); //wymuszanie zmiany typu na zmiennej
		
	}
	
}
