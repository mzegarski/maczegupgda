package stare;

import java.util.Scanner;

public class petle {

	public static void main(String[] args) {

		Scanner liczby = new Scanner(System.in);

		// fromOneToTen();

		// fromOneToTenVer2();

		// evenNumbersFromOneToTwenty();

		// fromLetterAtoZ();

		// from100To130DevidedBy3();

		// printingHelloFromUser();

		// printUntillNegativeNumber(liczby);

		 multiplicationTableToFiveTimesFive();

		liczby.close();

	}

	private static void multiplicationTableToFiveTimesFive() {
		for (int i = 1; i <= 5; i++) {
			for (int j = 1; j <= 5; j++) {
				System.out.println("" + i + " * " + j + " = " + (i * j));
			}
		}

	}

	private static void printUntillNegativeNumber(Scanner liczby) {
		int u;
		do {
			System.out.println("Podaj liczbe ujemna: ");
			u = liczby.nextInt();

		} while (u >= 0);
	}

	private static void printingHelloFromUser(Scanner liczby) {
		System.out.println("Podaj liczbe: ");
		liczby.close();
		int x = liczby.nextInt();
		for (int i = 0; i <= x; i++) {
			System.out.println("Hello World !");
		}
	}

	private static void from100To130DevidedBy3() {
		for (int i = 100; i <= 130; i++) {
			if (i % 3 == 0)
				System.out.println(i);
		}
	}

	private static void fromLetterAtoZ() {
		for (char a = 'a'; a <= 'z'; a++) {
			System.out.println(a);
		}
	}

	private static void evenNumbersFromOneToTwenty() {
		for (int i = 1; i <= 20; i++) {
			if (i % 2 == 0) {
				System.out.println(i);
			}
		}
	}

	private static void fromOneToTenVer2() {
		int n = 1;
		while (n <= 10) {
			System.out.println(n);
			n++;
		}
	}

	private static void fromOneToTen() {
		for (int i = 1; i <= 10; i++) {
			System.out.println(i);
		}
	}

}
