package stare;
public class MyMath {

	public static void main(String[] args) {
		System.out.println(pow(2, 1));
	}

	public static int max(int a, int b) {

		if (a >= b) {
			return a;
		} else {
			return b;
		}

	}

	public static long max(long a, long b) {

		if (a >= b) {
			return a;
		} else {
			return b;
		}

	}

	public static double max(double a, double b) {

		if (a >= b) {
			return a;
		} else {
			return b;
		}

	}

	public static float max(float a, float b) {

		if (a >= b) {
			return a;
		} else {
			return b;
		}

	}

	public static int min(int a, int b) {

		if (a <= b) {
			return a;
		} else {
			return b;
		}

	}

	public static double min(double a, double b) {

		if (a <= b) {
			return a;
		} else {
			return b;
		}

	}

	public static float min(float a, float b) {

		if (a <= b) {
			return a;
		} else {
			return b;
		}

	}

	public static long min(long a, long b) {

		if (a <= b) {
			return a;
		} else {
			return b;
		}

	}

	public static int abs(int a) {

		if (a >= 0) {
			return a;
		} else {
			return a * (-1);
		}
	}

	public static float abs(float a) {

		if (a >= 0) {
			return a;
		} else {
			return a * (-1);
		}
	}

	public static double abs(double a) {

		if (a >= 0) {
			return a;
		} else {
			return a * (-1);
		}
	}

	public static long abs(long a) {

		if (a >= 0) {
			return a;
		} else {
			return a * (-1);
		}
	}

	public static int pow(int a, int b) {

		int liczba = 1;
		for (int i = 0; i < b ; i++) {
			liczba *= a;}
		return liczba;

	}

	public static long pow(long a, int b) {

		long liczba = 1;
		for (int i = 0; i < b ; i++) {
			liczba *= a;}
		return liczba;

	}

	public static float pow(float a, int b) {

		float liczba = 1;
		for (int i = 0; i < b ; i++) {
			liczba *= a;}
		return liczba;
	}

	public static double pow(double a, int b) {

		
			double liczba = 1;
			for (int i = 0; i < b ; i++) {
				liczba *= a;}
			return liczba;
		}

	}


