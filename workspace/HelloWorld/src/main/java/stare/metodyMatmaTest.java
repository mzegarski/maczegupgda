package stare;

import org.junit.Test;

public class metodyMatmaTest {

	// publiczna, void, dowolna nazwa, brak parametrow, pami�ta� o @Test na pocz�tku!!!

	@Test
	public void testIloczynu() {

		assert metodyMatma.iloczyn(2, 2) == 4;
		assert metodyMatma.iloczyn(4, 4) == 16;
	}

	@Test
	public void testSuma() {

		int pierwszaLiczba = 1;
		int drugaLiczba = 2;
		int wynik = 3;

		assert metodyMatma.suma(pierwszaLiczba, drugaLiczba) == wynik;

	}

	@Test
	public void testMinFromArray() {

		int[] tablicaLiczb = { 1, 2, 3 };
		int wynik = 1;

		assert metodyMatma.minFromArray(tablicaLiczb) == wynik;
	}

	@Test
	public void testMinNumber() {

		int pierwszaLiczba = 1;
		int drugaLiczba = 2;
		int trzeciaLiczba = 3;
		int wynik = 1;

		assert metodyMatma.minNumber(pierwszaLiczba, drugaLiczba, trzeciaLiczba) == wynik;
	}
	
	@Test
	public void testSearchInArray(){
		
		int[] tablicaLiczb = { 1, 2, 3 };
		int szukana_Liczba = 1;
		int wynik = 3;
		
		assert metodyMatma.searchInArray(tablicaLiczb, szukana_Liczba) == wynik;
	}
	

}
