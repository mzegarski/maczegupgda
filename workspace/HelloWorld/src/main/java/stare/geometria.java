package stare;


public class geometria {

	public static double poleKwadratu(double bokKwadratu) {

		return bokKwadratu * bokKwadratu;
	}

	public static double poleSzescianu(double bokSzescianu) {

		return poleKwadratu(bokSzescianu * 6);
	}

	public static double poleKola(double promienKola) {

		return promienKola * promienKola * Math.PI;

	}

	public static double objetoscWalca(double promienWalca, double wysokoscWalca) {

		return Math.PI * promienWalca * promienWalca * wysokoscWalca;
	}

	public static double objetoscStozka(double promienStozka, double wysokoscStozka) {

		return objetoscWalca(promienStozka, wysokoscStozka) / 3;
	}

	public static double objetnoscSzescianu(double bokSzescianu) {

		return Math.pow(bokSzescianu, 3);
	}

	public static double objetoscOstroslupaPrawidlowego(double bokKwadratuPodstawy, double wysokoscOstroslupa) {
		;

		return Math.pow(bokKwadratuPodstawy, 2) * wysokoscOstroslupa / 3;
	}

}
