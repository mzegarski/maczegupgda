package stare;

import org.junit.Test;

public class MyMathTest {

	@Test
	public void testMax() {

		assert MyMath.max(1, 2) == 2;
		assert MyMath.max(1.0, 2.0) == 2;
		assert MyMath.max(1f, 2f) == 2f;
		assert MyMath.max(1L, 2L) == 2L;

	}
	
	@Test
	public void testmin() {

		assert MyMath.min(1, 2) == 1;
		assert MyMath.min(1.0, 2.0) == 1;
		assert MyMath.min(1f, 2f) == 1f;
		assert MyMath.min(1L, 2L) == 1L;

	}
	
	@Test
	public void testabs(){
		
		assert MyMath.abs(-5) == 5;
		assert MyMath.abs(-5.0) == 5;
		assert MyMath.abs(-5f) == 5f;
		assert MyMath.abs(-5L) == 5L;
	}
	
	@Test
	public void testpow() {

		assert MyMath.pow(2, 3) == 8;
		assert MyMath.pow(2.0, 3) == 8.0;
		assert MyMath.pow(2.0f, 3) == 8f;
		assert MyMath.pow(2L, 3) == 8L;
	}
}
