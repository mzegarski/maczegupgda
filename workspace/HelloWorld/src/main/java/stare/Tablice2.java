package stare;

import java.util.Scanner;

public class Tablice2 {

	public static void main(String[] args) {

		Scanner cs = new Scanner(System.in);

		// sumFromArray();

		// multiplicationArray();

		 silnia(cs);

		// charCounter(cs);

		boolean[] bool1 = { true, false, true, false, true };
		boolean[] bool2 = { true, true, true, true, true };
		boolean[] bool3 = { false, false, false, false, false };

		//koniunkcja(bool2);
	
		//alternatywa(bool3);

	}

	private static void alternatywa(boolean[] bool) {
		boolean wynik_alternatywy = false;
		for (int i = 0; i < bool.length; i++) {
			if (bool[i] == true) {
				wynik_alternatywy = true;
				break;
			}
		}

		System.out.println("Wynik alternatywy: " + wynik_alternatywy);
		System.out.println();
	}

	private static void koniunkcja(boolean[] bool) {
		boolean wynik_koniunkcji = true;

		for (int i = 0; i < bool.length; i++) {
			if (bool[i] == false) {
				wynik_koniunkcji = false;
				break;
			}
		}
		System.out.println("Wynik koniunkcji: " + wynik_koniunkcji);
		System.out.println();
	}

	private static void charCounter(Scanner cs) {
		System.out.println("Podaj napis: ");
		String napis = cs.next();
		System.out.println("Podaj znak: ");
		char znak = cs.next().charAt(0);

		int licznik = 0;
		for (int i = 0; i < napis.length(); i++) {
			if (napis.charAt(i) == znak) {
				licznik++;
			}

		}
		System.out.println("Ilosc znak�w " + znak + " w slowie " + napis + " wynosi: " + licznik);
	}

	private static void silnia(Scanner cs) {
		System.out.println("Podaj silnie: ");
		int s = cs.nextInt();

		System.out.println(liczSilnie(s));

		cs.close();
	}
	
	private static int liczSilnie(int s){
		
		int silnia = 1;

		for (int i = 1; i <= s; i++) {

			silnia = silnia * i;

		}
		return silnia;
		
	}

	private static void multiplicationArray() {
		int[] tab1 = { 2, 3, 10 };
		int suma = 1;
		for (int tablica : tab1) {
			suma *= tablica;
		}
		System.out.println("Suma liczb z tablicy to: " + suma);
	}

	private static void sumFromArray() {
		int[] tab1 = { 1, 3, 2, 1, 3, 5, 2, 3 };
		int suma = 0;
		for (int tablica : tab1) {
			suma += tablica;
		}
		System.out.println("Suma liczb z tablicy to: " + suma);
	}

}
