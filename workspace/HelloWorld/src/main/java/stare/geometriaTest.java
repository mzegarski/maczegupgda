package stare;

import org.junit.Test;

public class geometriaTest {

	@Test
	public void testPoleKwadratu() {

		double bokKwadratu = 2;
		double wynik = 4;

		assert geometria.poleKwadratu(bokKwadratu) == wynik;
	}

	@Test
	public void testPoleSzescianu() {

		double bokSzescianu = 10;
		double wynik = 600;

		assert geometria.poleSzescianu(bokSzescianu) == wynik;

	}

	@Test
	public void testPoleKola() {

		double promienKola = 2;
		double wynik = Math.PI * 4;

		assert geometria.poleKola(promienKola) == wynik;

	}

	@Test
	public void testObjetoscWalca() {

		double promienWalca = 2;
		double wysokoscWalca = 5;
		double wynik = Math.PI * 20;

		assert geometria.objetoscWalca(promienWalca, wysokoscWalca) == wynik;

	}

	@Test
	public void testObjetoscStozka() {

		double promienStozka = 2;
		double wysokoscStozka = 5;
		double wynik = (Math.PI * 20)/3;

		assert geometria.objetoscStozka(promienStozka, wysokoscStozka) == wynik;

	}

	@Test
	public void testObjetoscSzescianu() {

		double bokSzescianu = 2;
		double wynik = 8;

		assert geometria.objetnoscSzescianu(bokSzescianu) == wynik;

	}

	@Test
	public void testObjetoscOstroslupaPrawidlowego() {

		double bokKwadratuPodstawy = 5;
		double wysokoscOstroslupa = 5;
		double wynik = 10;

		assert geometria.objetoscOstroslupaPrawidlowego(bokKwadratuPodstawy, wysokoscOstroslupa) == wynik;

	}

}
