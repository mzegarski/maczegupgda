package stare;

import org.junit.Test;

public class metodyMatma {

	public static void main(String[] args) {
		int a = 2;
		int b = 3;
		int c = 4;
		int[] tab1 = { 5, 6, 7, 2, 3, 4, 1 };
		int s = 7;

		System.out.println("Wynik dodawania: " + suma(a, b));

		System.out.println("Wynik mno�enia: " + iloczyn(a, b));

		System.out.println("Najmniejsza liczba z " + a + " i " + b + " to: " + minNumber(a, b));

		System.out.println("Najmniejsza liczba z " + a + " i " + b + " i " + c + " to: " + minNumber(a, b, c));

		System.out.println("Najmniejsza liczba z tablicy to: " + minFromArray(tab1));

		System.out.println("Pozycja szukanej liczby w tablicy: " + searchInArray(tab1, s));

	}

	public static int iloczyn(int pierwszaLiczba, int drugaLiczba) {
		int iloczyn = pierwszaLiczba * drugaLiczba;
		return iloczyn;
	}

	public static int suma(int pierwszaLiczba, int drugaLiczba) {
		int suma = pierwszaLiczba + drugaLiczba;
		return suma;

	}

	public static int minNumber(int pierwszaLiczba, int drugaLiczba) {
		if (pierwszaLiczba < drugaLiczba) {
			return pierwszaLiczba;
		} else {
			return drugaLiczba;
		}

	}

	public static int minNumber(int pierwszaLiczba, int drugaLiczba, int trzeciaLiczba) {
		int pierwszaIDruga = minNumber(pierwszaLiczba, drugaLiczba);
		int pierwszaiDrugaITrzecia = minNumber(pierwszaIDruga, trzeciaLiczba);

		return pierwszaiDrugaITrzecia;

	}

	public static int minNumberVER2(int pierwszaLiczba, int drugaLiczba, int trzeciaLiczba) {

		if (minNumber(pierwszaLiczba, drugaLiczba) < minNumber(drugaLiczba, trzeciaLiczba)) {
			return minNumber(pierwszaLiczba, drugaLiczba);
		} else {
			return minNumber(drugaLiczba, trzeciaLiczba);
		}

	}

	public static int minFromArray(int[] tablicaLiczb) {

		int min = tablicaLiczb[0];

		for (int i = 1; i < (tablicaLiczb.length); i++) {

			if (min > tablicaLiczb[i]) {
				min = tablicaLiczb[i];
			}

		}

		return min;
	}

	public static int searchInArray(int[] tablicaLiczb, int szukana_Liczba) {

		for (int i = 0; i < (tablicaLiczb.length); i++) {

			if (szukana_Liczba == tablicaLiczb[i]) {
				return i + 1;

			}

		}

		return -1;

	}
	


}
