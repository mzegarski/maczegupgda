
package stare;
import java.util.Scanner;

public class Tablice {

	public static void main(String[] args) {

		Scanner cs = new Scanner(System.in);

		// tabliceStart();

		// fourElementTablePrinting();

		// stringOneByOne();

		// intArrayByUser(cs);

		// namesPrinting();

		cs.close();

	}

	private static void namesPrinting() {
		String[] names = { "Ania", "Kasia", "Zosia", "Marysia", "Ewelina", "And�elika" };

		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}

		System.out.println();

		for (String i : names) {
			System.out.println(i);
		}

		System.out.println();

		for (int i = 0; i < names.length; i = i + 2) {
			System.out.println(names[i]);
		}

		System.out.println();

		for (int i = 0; i < names.length; i++) {
			if (names[i].charAt(0) == 'A') {
				System.out.println(names[i]);
			}
		}
	}

	private static void intArrayByUser(Scanner cs) {
		System.out.println("Podaj d�ugo�� tablicy: ");
		int dlugosc_tablicy = cs.nextInt();

		int[] user_array = new int[dlugosc_tablicy];

		for (int i = 0; i < user_array.length; i++) {
			System.out.println("Podaj element tablicy nr " + (i + 1) + ": ");
			user_array[i] = cs.nextInt();
		}
	}

	private static void stringOneByOne() {
		String napis = "Hello World";

		for (int i = 0; i < napis.length(); i++) {
			System.out.println(napis.charAt(i));
		}
	}

	private static void fourElementTablePrinting() {
		int[] tablicaFour = { 1, 3, 5, 10 };

		for (int i = 0; i < tablicaFour.length; i++) {
			System.out.println(tablicaFour[i]);
		}

		System.out.println();

		for (int i = tablicaFour.length - 1; i >= 0; i--) {
			System.out.println(tablicaFour[i]);
		}

		System.out.println();

		for (int nazwa : tablicaFour) {
			System.out.println(nazwa);
		}

	}

	private static void tabliceStart() {
		int[] liczby = { 2, 3 };

		int[] liczby100 = new int[100]; // tablica stu zer od 0 do 99

		boolean[] boole = new boolean[2];

		String[] slowa = new String[10];

		char[] znaki = new char[3];

		System.out.println(liczby[1]);
		System.out.println(liczby100[1]);
		System.out.println(boole[1]);
		System.out.println(slowa[1]);
		System.out.println(znaki[1]);
	}

}
