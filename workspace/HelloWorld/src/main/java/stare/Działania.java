package stare;


public class Dzia�ania {

	public static void main(String[] args) {
		
		int a = 2+3;
		System.out.print("2 + 3: ");
		System.out.println(a);
		
		int b =2-4;
		System.out.print("2 - 4: ");
		System.out.println(b);
		
		int c = 5/2;
		System.out.print("5 / 2: ");
		System.out.println(c);
		
		double d = 5.0/2;
		System.out.print("5.0 / 2: ");
		System.out.println(d);
		
		System.out.print("5.0 / 2.0: ");
		System.out.println(5.0/2.0);
		
		System.out.print("100L - 10: ");
		System.out.println(100L - 10);
		
		System.out.print("2f - 3: ");
		System.out.println(2f - 3);
		
		System.out.print("5f / 2: ");
		System.out.println(5f / 2);
		
		System.out.print("5d / 2: ");
		System.out.println(5d / 2);
		
		System.out.print("'A' + 2: ");
		System.out.println('A' + 2);
		
		System.out.print("'a' + 2: ");
		System.out.println('a' + 2);
		
		System.out.print("\"a\" + 2: ");
		System.out.println("a" + 2);
		
		System.out.print("'a' + 'b': ");
		System.out.println('a' + 'b');
		
		System.out.print("\"a\" + 'b' + 3: ");
		System.out.println("a" + 'b' + 3);
		
		System.out.print("'b' + 3 + \"a\": ");
		System.out.println('b' + 3 + "a");
		
		System.out.print("9%4: ");
		System.out.println(9%4);
		
		int z1 = 9;
		int z2 = 4;
		
		System.out.println(z1-z2*(z1/z2));

	}

}
