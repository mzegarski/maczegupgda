import java.util.Scanner;

public class PrimeNumbersMain {

	public static void main(String[] args) {

		
		//14492201
		Scanner numScan = new Scanner(System.in);
		System.out.println("Program sprawdza czy podana liczba jest liczba pierwsza.");
		System.out.print("Podaj liczbe do sprawdzenia: ");
		int num = numScan.nextInt();
		numScan.close();

		boolean isNumPrime = true;

		for (int i = 2; i < num; i++) {
			System.out.println("Probuje podzielic przez: " + i);

			if (num % i == 0) {

				System.out.println("Nie ma reszty z dzielenia.");
				isNumPrime = false;
				break;
			} else {
				System.out.println("Jest reszta z dzielenia.");
				isNumPrime = true;

			}
		}

		if (isNumPrime == true)
			System.out.println("Liczba " + num + " jest liczba pierwsza.");
		else
			System.out.println("Liczba " + num + " nie jest liczba pierwsza.");

	}

}
