package pl.org.pfig.cars;

public class Truck extends Car {

	private String name;
	private String engine;
	private int tires;

	public Truck(String name, String engine, int tires) {
		super();
		this.name = name;
		this.engine = engine;
		this.tires = tires;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getTires() {
		return this.tires;
	}

}
