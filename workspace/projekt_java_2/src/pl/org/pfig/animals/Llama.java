package pl.org.pfig.animals;

public class Llama implements AnimalInterface, Soundable {

	private String name;
	
	public Llama(String name){
		this.name = name;
	}
	
	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}
	
	public String getSound() {
		return "lam lam";
	}

}
