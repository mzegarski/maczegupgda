package pl.org.pfig.animals;

public class Dog implements AnimalInterface {

	private String name;

	public Dog(String name) {
		super(); // wywolanie konstruktora z klasy nadrzednej
		this.name = name; //tutaj musi byc this.name zeby nie doszlo do dwuznacznosci
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Dog: " +this.name;
	}

}
