package pl.org.pfig.animals;

public interface Soundable {

	public String getSound();

}
