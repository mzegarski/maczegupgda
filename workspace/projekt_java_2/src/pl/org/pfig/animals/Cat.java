package pl.org.pfig.animals;

public class Cat implements AnimalInterface, Soundable {

	private String name;

	public Cat(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public String getSound() {
		return "miau miau";
	}

}
