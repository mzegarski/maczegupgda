package pl.org.pfig.animals;

public interface AnimalInterface {

	public String getName();
	
	public default int getLegs(){
		return 4;
	}

}
