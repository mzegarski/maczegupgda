package tdd;

public class Exercises15 {

	public int absoluteValue(int number) {
		// 1. Napisz funkcję zwracającą wartość bezwzględną liczby.
		if (number < 0)
			number *= -1;

		return number;
	}

	public String nameOfAWeekDay(int number) throws Exception {

		// 2. Napisz funkcję, która dla zadanego dnia wyświetli jego dzień
		// tygodnia
		// (np. 1 - niedziela).
		// (należy zbadać czy liczba mieści się w zakresie)
		if (number == 1)
			return "Sunday";

		if (number == 2)
			return "Monday";

		if (number == 3)
			return "Tuesday";

		if (number == 4)
			return "Wednesday";

		if (number == 5)
			return "Thursday";

		if (number == 6)
			return "Friday";

		if (number == 7)
			return "Saturday";

		else
			throw new IllegalArgumentException("Only 7 days in a week");

	}

	public String nameofMonth(int month) throws Exception {

		if (month < 1 || month > 12) {

			throw new IllegalArgumentException("Bad input value");
		}

		String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };

		return months[month - 1];

	}

	public int euklides(int a, int b) {
		// 4. Napisz funkcję licząca NWD wykorzystując algorytm Euklidesa.

		int c; // Tworzymy zmienną c o typie int
		while (b != 0) // Tworzymy pętlę działającą gdy b ≠ 0
		{
			c = a % b; // Zmienna c przyjmuje wartość a modulo b
			a = b; // Przypisujemy b do a
			b = c; // Przypisujemy c do b
		}
		return a;

	}

	public int timeToSeconds(int g, int m, int s) {
		// 5. Napisz funkcję, która przyjmuje jako argumenty: g - godziny, m -
		// minuty, s - sekundy, a następnie zwraca podany czas w sekundach.
		if (g < 0 || m < 0 || s < 0) {
			throw new IllegalArgumentException("Bad input value");
		}

		m = m + g * 60;
		s = s + m * 60;

		return s;
	}

	public int[] printEvenAndOddNumber(int a, int b) {
		// Użytkownik podaje dwie liczby całkowite a, b. algorytm ma za zadanie
		// wypisać wszystkie parzyste liczby w kolejności rosnącej, a następnie
		// wszystkie liczby nieparzyste w kolejności malejącej z przedziału
		// <a;b>. niech a, b –liczby całkowite z zakresu 0-255. Np. dla danych
		// wejściowych a=3, b=8,otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3.

		if (a < 0 || b < 0 || a > 255 || b > 255) {
			throw new IllegalArgumentException("Bad input value");
		}

		int[] numberTab = new int[b - a + 1];

		int i = 0;

		int aTemp = a;

		for (aTemp = (aTemp % 2 != 0) ? ++aTemp : aTemp; aTemp <= b; i++) {
			numberTab[i] = aTemp;
			aTemp += 2;
		}

		if (b % 2 == 0)
			b--;

		for (; b >= a; i++) {
			numberTab[i] = b;
			b -= 2;
		}

		return numberTab;

	}

	public int sumOfSquareFromOneToNumber(int number) {

		// 7. Napisz funkcję, która dla zadanej liczby zwróci sumę kwadratów
		// poszczególnych liczb od 1 do zadanej liczby. Przyjmij i zbadaj czy
		// użytkownik przekazał liczbę w przedziale <0, 10>

		if (number < 0 || number > 10) {
			throw new IllegalArgumentException("Bad input value");
		}

		int sum = 0;
		if (number == 0)
			return sum;

		for (int i = 1; i <= number; i++) {
			sum += i * i;
		}

		return sum;
	}

	public void moneyInBank(double money, double percent, int years) {

		// Kasia ulokowała w banku pewna ilość złotych na okres jednego roku.
		// Oprocentowanie roczne w tym banku wynosi 19,4%. Napisz algorytm,
		// który
		// będzie obliczał ilość pieniędzy na koncie po jednym roku dla dowolnej
		// sumy pieniędzy. Zmodyfikuj program tak, aby obliczał kwotę dla
		// wczytanej
		// liczby lat.

		double podatekBelki = 0.19;
		double zysk = 0;

		if (percent > 1)
			percent /= 100;
		
		double moneyStart = money;

		System.out.println("Oprocentowanie wynosi " + percent + " %. Masz " + money + ".");
		System.out.println("Program uwzglednia podatek od zysku kapitalowego wynoszacy: " +podatekBelki +"%. \n");
		for (int i = 0; i < years; i++) {

			zysk = money * percent;

			System.out.print("Otrzymalbys na poczatku " + (i + 1) + " roku ");
			System.out.printf("%.2f", zysk);
			System.out.println();
			System.out.print("Ale po odjeciu podatku otrzymasz: ");
			zysk -= zysk * podatekBelki;
			System.out.printf("%.2f", zysk);
			System.out.println();
			System.out.print("Razem z swoim kapitalem masz ");
			money += zysk;
			System.out.printf("%.2f", money);
			System.out.println(" po " + (i + 1) + " roku.");
		}
		
		
		
		System.out.println("Zarobiono: " +(money-moneyStart) +" pieniazkow.");
		System.out.println("Jest to " +(money/moneyStart -1) +"% zysku.");

	}

}
