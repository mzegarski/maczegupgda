package tdd;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class timeToSecondsMethod {

	private Exercises15 ex;

	@Before
	public void doStuff() {

		ex = new Exercises15();
	}

	@Test(expected = Exception.class)
	public void negativeHourGivenExeptedException() {

		int g = -1;
		int m = 1;
		int s = 1;
		ex.timeToSeconds(g, m, s);
	}

	@Test(expected = Exception.class)
	public void negativeMinuteGivenExeptedException() {

		int g = 1;
		int m = -1;
		int s = 1;
		ex.timeToSeconds(g, m, s);
	}

	@Test(expected = Exception.class)
	public void negativeSecondGivenExeptedException() {

		int g = 1;
		int m = 1;
		int s = -1;
		ex.timeToSeconds(g, m, s);
	}

	@Test
	public void takeRandomHourAndCheckIfConvertedProperly() {

		Random rand = new Random();

		int g = rand.nextInt(100);
		int m = 0;
		int s = 0;

		assertEquals(g * 3600, ex.timeToSeconds(g, m, s));

	}

	@Test
	public void takeRandomMinuteAndCheckIfConvertedProperly() {

		Random rand = new Random();

		int g = 0;
		int m = rand.nextInt(100);
		int s = 0;

		assertEquals(m * 60, ex.timeToSeconds(g, m, s));

	}

	@Test
	public void takeSecondAndCheckIfConvertedProperly() {

		Random rand = new Random();

		int g = 0;
		int m = 0;
		int s = rand.nextInt(100);

		assertEquals(s, ex.timeToSeconds(g, m, s));

	}

}
