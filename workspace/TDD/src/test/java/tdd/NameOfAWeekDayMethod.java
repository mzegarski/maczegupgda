package tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NameOfAWeekDayMethod {

	private Exercises15 ex;

	@Before
	public void doStuff() {

		ex = new Exercises15();
	}

	@Test
	public void numberOneGivenExpectedSunday() throws Exception {

		int i = 1;

		assertEquals("Sunday", ex.nameOfAWeekDay(i));

	}

	@Test
	public void numberTwoGivenExpectedMonday() throws Exception {

		int i = 2;

		assertEquals("Monday", ex.nameOfAWeekDay(i));

	}

	@Test
	public void numberThreeGivenExpectedTuesday() throws Exception {

		int i = 3;

		assertEquals("Tuesday", ex.nameOfAWeekDay(i));

	}

	@Test
	public void numberFourGivenExpectedWednesday() throws Exception {

		int i = 4;

		assertEquals("Wednesday", ex.nameOfAWeekDay(i));

	}

	@Test
	public void numberFiveGivenExpectedThursday() throws Exception {

		int i = 5;

		assertEquals("Thursday", ex.nameOfAWeekDay(i));

	}

	@Test
	public void numberSixGivenExpectedFriday() throws Exception {

		int i = 6;

		assertEquals("Friday", ex.nameOfAWeekDay(i));

	}

	@Test
	public void numberSevenGivenExpectedSaturday() throws Exception {

		int i = 7;

		assertEquals("Saturday", ex.nameOfAWeekDay(i));

	}

	@Test(expected = Exception.class)
	public void numberSmallerThanOneGivenExpectedException() throws Exception {

		int i = 0;

		ex.nameOfAWeekDay(i);

	}

	@Test(expected = Exception.class)
	public void numberBiggerThanSevenGivenExpectedException() throws Exception {

		int i = 8;

		ex.nameOfAWeekDay(i);

	}

}
