package tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AbsoluteValueMethod {

	private Exercises15 ex;

	@Before
	public void doStuff() {

		ex = new Exercises15();
	}

	@Test
	public void positiveNumberGivenExpectedPositive() {

		int a = 5;

		assertEquals(a, ex.absoluteValue(a));

	}

	@Test
	public void negativeNumberGivenExpectedPositive() {

		int a = 5;

		assertEquals(a, ex.absoluteValue(a));

	}

	@Test
	public void zeroGivenExpectedZero() {

		int a = 0;

		assertEquals(a, ex.absoluteValue(a));
	}

}
