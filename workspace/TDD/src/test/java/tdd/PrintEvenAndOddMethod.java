package tdd;

import java.util.Random;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PrintEvenAndOddMethod {

	private Exercises15 ex;

	// Użytkownik podaje dwie liczby całkowite a, b. algorytm ma za zadanie
	// wypisać wszystkie parzyste liczby w kolejności rosnącej, a następnie
	// wszystkie liczby nieparzyste w kolejności malejącej z przedziału
	// <a;b>. niech a, b –liczby całkowite z zakresu 0-255. Np. dla danych
	// wejściowych a=3, b=8,otrzymujemy plik wynikowy: 4, 6, 8, 7, 5, 3.

	@Before
	public void doStuff() {

		ex = new Exercises15();
	}

	@Test(expected = Exception.class)
	public void FirstNumberNegativeExpectException() {

		int a[] = { -1, -1000, -5000};
		int b = 50;

		for (int i : a)

			ex.printEvenAndOddNumber(i, b);

	}

	@Test(expected = Exception.class)
	public void SecondNumberNegativeAlsoSecondNumberSmallerThanFirstExpectException() {

		int a = 50;
		int b = -1;

		ex.printEvenAndOddNumber(a, b);

	}

	@Test(expected = Exception.class)
	public void FirstNumberTooBigAlsoSecondNumberSmallerThanFirstExpectException() {

		int a = 256;
		int b = 50;

		ex.printEvenAndOddNumber(a, b);

	}

	@Test(expected = Exception.class)
	public void SecondNumberTooBigExpectException() {

		int a = 50;
		int b = 256;

		ex.printEvenAndOddNumber(a, b);

	}

	@Test(expected = Exception.class)
	public void SecondNumberBiggerThanFirstExpectException() {

		Random randy = new Random();

		int a = 100 + randy.nextInt(1337);
		int b = 100;

		ex.printEvenAndOddNumber(a, b);

	}

	@Test
	public void TwoNumbersGivenExpectedToReturnProperArray() {

		int a = 3;
		int b = 8;

		int[] expected = { 4, 6, 8, 7, 5, 3 };

		assertArrayEquals(expected, ex.printEvenAndOddNumber(a, b));

	}

	@Test
	public void TwoOtherNumbersGivenExpectedToReturnProperArray() {

		int a = 2;
		int b = 57;

		int[] expected = { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48,
				50, 52, 54, 56, 57, 55, 53, 51, 49, 47, 45, 43, 41, 39, 37, 35, 33, 31, 29, 27, 25, 23, 21, 19, 17, 15,
				13, 11, 9, 7, 5, 3 };

		assertArrayEquals(expected, ex.printEvenAndOddNumber(a, b));

	}

	@Test
	public void TwoSameNumbersGivenExpectedToReturnArrayWithSingleSameNumber() {

		int a = 10;

		int[] expected = { a };

		assertArrayEquals(expected, ex.printEvenAndOddNumber(a, a));
	}

	@Test
	public void TwoNumbersGivenFirstMinSecondMaxExpectedToRetrunProperArray() {

		int a = 0;
		int b = 255;

		int[] expected = { 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46,
				48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 68, 70, 72, 74, 76, 78, 80, 82, 84, 86, 88, 90, 92, 94, 96, 98,
				100, 102, 104, 106, 108, 110, 112, 114, 116, 118, 120, 122, 124, 126, 128, 130, 132, 134, 136, 138, 140,
				142, 144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 166, 168, 170, 172, 174, 176, 178, 180, 182,
				184, 186, 188, 190, 192, 194, 196, 198, 200, 202, 204, 206, 208, 210, 212, 214, 216, 218, 220, 222, 224,
				226, 228, 230, 232, 234, 236, 238, 240, 242, 244, 246, 248, 250, 252, 254, 255, 253, 251, 249, 247, 245,
				243, 241, 239, 237, 235, 233, 231, 229, 227, 225, 223, 221, 219, 217, 215, 213, 211, 209, 207, 205, 203,
				201, 199, 197, 195, 193, 191, 189, 187, 185, 183, 181, 179, 177, 175, 173, 171, 169, 167, 165, 163, 161,
				159, 157, 155, 153, 151, 149, 147, 145, 143, 141, 139, 137, 135, 133, 131, 129, 127, 125, 123, 121, 119,
				117, 115, 113, 111, 109, 107, 105, 103, 101, 99, 97, 95, 93, 91, 89, 87, 85, 83, 81, 79, 77, 75, 73, 71,
				69, 67, 65, 63, 61, 59, 57, 55, 53, 51, 49, 47, 45, 43, 41, 39, 37, 35, 33, 31, 29, 27, 25, 23, 21, 19,
				17, 15, 13, 11, 9, 7, 5, 3, 1 };

		assertArrayEquals(expected, ex.printEvenAndOddNumber(a, b));

	}

}
