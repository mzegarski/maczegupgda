package tdd;

import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class NameOfAMonthMethod {

	private Exercises15 ex;

	@Before
	public void doStuff() {

		ex = new Exercises15();
	}

	@Test
	public void numberOneToTwelveGivenExceptedToReturnProperMonthNameButIfOtherNumberExpectedToThrowException()
			throws Exception {

		int[] actualTab = new int[1000];
		Random rand = new Random();

		for (int i = 0; i < actualTab.length; i++)
			actualTab[i] = rand.nextInt(20) - 5;

		IllegalArgumentException iae = null;

		for (int t : actualTab) {

			try {

				if (t == 1)
					assertEquals("January", ex.nameofMonth(t));
				else if (t == 2)
					assertEquals("February", ex.nameofMonth(t));
				else if (t == 3)
					assertEquals("March", ex.nameofMonth(t));
				else if (t == 4)
					assertEquals("April", ex.nameofMonth(t));
				else if (t == 5)
					assertEquals("May", ex.nameofMonth(t));
				else if (t == 6)
					assertEquals("June", ex.nameofMonth(t));
				else if (t == 7)
					assertEquals("July", ex.nameofMonth(t));
				else if (t == 8)
					assertEquals("August", ex.nameofMonth(t));
				else if (t == 9)
					assertEquals("September", ex.nameofMonth(t));
				else if (t == 10)
					assertEquals("October", ex.nameofMonth(t));
				else if (t == 11)
					assertEquals("November", ex.nameofMonth(t));
				else if (t == 12)
					assertEquals("December", ex.nameofMonth(t));

			} catch (IllegalArgumentException e) {
				iae = e;
			}
		}

	}
}
