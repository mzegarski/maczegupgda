package cities;

import cities.connector.DataBaseConnector;
import cities.model.City;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zegarski on 2017-07-06.
 */
public class Main {
    public static void main(String[] args) {

//        DB_Execute("INSERT INTO `city` VALUES (id, 'Gdańsk')");
//        DB_Execute("INSERT INTO `city` VALUES (id, 'Gdynia')");
//        DB_Execute("INSERT INTO `city` VALUES (id, 'Rumia')");
//        DB_Execute("INSERT INTO `city` VALUES (id, 'Sopot')");
//        DB_Execute("INSERT INTO `city` VALUES (id, 'Wejherowo')");
//        DB_Execute("INSERT INTO `city` VALUES (id, 'Reda')");
//        DB_Execute("UPDATE city SET name = 'DESTROYED' WHERE name != 'Gdańsk' ");
//        DB_Execute("DELETE FROM city WHERE name != 'Gdańsk'");
//              DB_Execute("DELETE FROM city WHERE 1");


        List<City> cityList = new ArrayList<>();

        try {
            Connection connection = DataBaseConnector.getInstance().getConecction();
            String query = "SELECT * FROM `city`";

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                cityList.add(new City(resultSet.getInt("id"), resultSet.getString("name")));

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (City c : cityList)
            System.out.println(c.getCity());

    }


    private static void DB_Execute(String sql) {
        try {
            Connection c = DataBaseConnector.getInstance().getConecction();
            Statement s = c.createStatement();
            s.execute(sql);
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void ManualDataBaseConnection() {
        //      dohost, doport, dbname, dbuser, dbpass
        String url = "jdbc:mysql://localhost:3306/rental";
        //String url ="jdbc:mysql://localhost:3306/rental?autoReconnect=true&useSSL=false";
        String user = "root";
        String pass = "admin";

//        1. Połączenie
//        2. Operacja
//        3. Rozlaczenie
        try {
            Connection connection = DriverManager.getConnection(url, user, pass);
            String query = "SELECT * FROM `city`";

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next())
                System.out.println(resultSet.getInt("id") + " " + resultSet.getString("name"));

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
