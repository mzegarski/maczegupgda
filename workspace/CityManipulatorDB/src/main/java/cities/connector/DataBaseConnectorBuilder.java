package cities.connector;

/**
 * Created by zegarski on 2017-07-06.
 */
public class DataBaseConnectorBuilder {

    private String dbhost = "localhost";
    private String dbname;
    private String dbuser = "root";
    private String dbpass = "admin";
    private int dbport = 3306;

    public DataBaseConnectorBuilder() {
    }

    public String hostname() {
        return dbhost;
    }

    public String database() {
        return dbname;
    }

    public String username() {
        return dbuser;
    }

    public String password() {
        return dbpass;
    }

    public int port() {
        return dbport;
    }

    public DataBaseConnectorBuilder hostname(String dbhost) {
        this.dbhost = dbhost;
        return this;
    }

    public DataBaseConnectorBuilder database(String dbname) {
        this.dbname = dbname;
        return this;
    }

    public DataBaseConnectorBuilder username(String dbuser) {
        this.dbuser = dbuser;
        return this;
    }

    public DataBaseConnectorBuilder password(String dbpass) {
        this.dbpass = dbpass;
        return this;
    }

    public DataBaseConnectorBuilder port(int dbport) {
        this.dbport = dbport;
        return this;
    }


}
