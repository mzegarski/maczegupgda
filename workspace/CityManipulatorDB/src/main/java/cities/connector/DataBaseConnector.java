package cities.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by zegarski on 2017-07-06.
 */
public class DataBaseConnector {

    private static DataBaseConnector _instance = null;
    private Connection _connection;

    private DataBaseConnector(DataBaseConnectorBuilder dbb) throws SQLException {
        String url = "jdbc:mysql://" + dbb.hostname() + ":" + dbb.port() + "/" + dbb.database();
        _connection = DriverManager.getConnection(url, dbb.username(), dbb.password());
    }

    public static DataBaseConnector getInstance() throws SQLException {
        if (_instance == null)
            _instance = new DataBaseConnector(
                    new DataBaseConnectorBuilder()
                            .hostname("localhost")
                            .port(3306)
                            .username("root")
                            .password("admin")
                            .database("rental"));
        return _instance;

    }

    public Connection getConecction() {
        return _connection;
    }

}
