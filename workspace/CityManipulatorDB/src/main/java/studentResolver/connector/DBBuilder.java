package studentResolver.connector;

/**
 * Created by zegarski on 2017-07-06.
 */
public class DBBuilder {


    private String dbhost = "localhost";
    private String dbname;
    private String dbuser = "root";
    private String dbpass = "admin";
    private int dbport = 3306;

    public DBBuilder() {
    }

    public String hostname() {
        return dbhost;
    }

    public String database() {
        return dbname;
    }

    public String username() {
        return dbuser;
    }

    public String password() {
        return dbpass;
    }

    public int port() {
        return dbport;
    }

    public DBBuilder hostname(String dbhost) {
        this.dbhost = dbhost;
        return this;
    }

    public DBBuilder database(String dbname) {
        this.dbname = dbname;
        return this;
    }

    public DBBuilder username(String dbuser) {
        this.dbuser = dbuser;
        return this;
    }

    public DBBuilder password(String dbpass) {
        this.dbpass = dbpass;
        return this;
    }

    public DBBuilder port(int dbport) {
        this.dbport = dbport;
        return this;
    }

}
