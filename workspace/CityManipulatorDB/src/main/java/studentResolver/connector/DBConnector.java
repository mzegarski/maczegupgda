package studentResolver.connector;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by zegarski on 2017-07-06.
 */
public class DBConnector implements AutoCloseable {



    private static DBConnector _instance = null;
    private Connection _connection;

    private DBConnector(DBBuilder dbb) throws SQLException {
        String url = "jdbc:mysql://" + dbb.hostname() + ":" + dbb.port() + "/" + dbb.database();
        _connection = DriverManager.getConnection(url, dbb.username(), dbb.password());
    }

    public static DBConnector getInstance() throws SQLException {
        if (_instance == null)
            _instance = new DBConnector(
                    new DBBuilder()
                            .hostname("localhost")
                            .port(3306)
                            .username("root")
                            .password("admin")
                            .database("students"));
        return _instance;

    }

    public Connection getConecction() {
        return _connection;
    }

    @Override
    public void close() throws Exception {

    }
}
