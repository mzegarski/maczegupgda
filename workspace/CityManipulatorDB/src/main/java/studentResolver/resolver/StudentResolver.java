package studentResolver.resolver;

import studentResolver.connector.DBConnector;

import java.sql.*;
import java.util.*;

/**
 * Created by zegarski on 2017-07-06.
 */

public class StudentResolver extends AbstractResolver<Student> {

    private Map<Integer, Student> studentsMap = new HashMap<>();

    public void start() {

        get();
        printMenu();

        try (Scanner sc = new Scanner(System.in)) {

            do {

                System.out.println("Twoj wybór: ");
                int choice = sc.nextInt();

                if (choice == 0) printMenu();

                else if (choice == 1) {
                    System.out.println("Podaj index do wydrukowania lub zero dla wszystkich");
                    choice = sc.nextInt();
                    if (choice == 0)
                        for (Student s : get()) System.out.println(s.toString());
                    else if (get(choice) != null)
                        System.out.println(get(choice).toString());
                    else
                        System.out.println("Nie ma takiego rekordu");

                } else if (choice == 2) {

                    Map<String, String> tempMap = new HashMap<>();
                    System.out.println("Podaj imię studenta: ");
                    String tempName = sc.next();
                    System.out.println("Podaj nazwisko studenta: ");
                    String tempLastName = sc.next();

                    tempMap.put(tempName, tempLastName);

                    insert(tempMap);




                } else if (choice == 3) {
                    System.out.println("Podaj index do usuniecia");
                    choice = sc.nextInt();
                    if (delete(choice))
                        System.out.println("Usunięto poprawnie rekord " + choice);
                    else
                        System.out.println("Nie usunięto rekordu.");

                } else if (choice == 4) {
                    System.out.println("4");

                } else if (choice == 5) {
                    System.out.println("Dowidzenia!");
                    System.exit(0);
                } else {
                    System.out.println("Wpisano niepoprawną wartość. Spróbuj ponownie: ");
                }
            } while (true);

        } catch (InputMismatchException e) {
            System.out.println("[ERROR] Można podać tylko cyfry.");
        }
    }

    private void printMenu() {
        System.out.println("0.Menu");
        System.out.println("1.Pokaż rekordy");
        System.out.println("2.Dodaj rekord");
        System.out.println("3.Usuń rekord [id]");
        System.out.println("4.Nadpisz rekord [id]");
        System.out.println("5.Koniec");
    }

    @Override
    public List<Student> get() {

        List<Student> studentList = new ArrayList<>();

        try (Connection connection = DBConnector.getInstance().getConecction()) {

            String query = "SELECT * FROM `student`";

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {

                Student s = new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getString("lastname"));

                studentsMap.put(resultSet.getInt("id"), s);
                studentList.add(s);

            }
            resultSet.close();
            statement.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return studentList;
    }

    @Override
    public Student get(int id) {

        Student s = new Student();

        try (Connection connection = DBConnector.getInstance().getConecction()) {

            String query = "SELECT * FROM `student` WHERE id = " + id;


            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);


            while (resultSet.next()) {


                System.out.println(resultSet.getString("name"));

                s.setId(resultSet.getInt("id"));
                s.setName(resultSet.getString("name"));
                s.setLastname(resultSet.getString("lastname"));

                studentsMap.put(resultSet.getInt("id"), s);
            }
            resultSet.close();
            statement.close();

        } catch (SQLException e) {
            //  e.printStackTrace();
            return null;
        }

        return s;
    }

    @Override
    public boolean delete(int id) {

        try (Connection connection = DBConnector.getInstance().getConecction()) {

            String query = "DELETE FROM `student` WHERE id = " + id;
            Statement statement = connection.createStatement();
            statement.execute(query);
            studentsMap.remove(id);
            statement.close();

        } catch (SQLException e) {
            //  e.printStackTrace();
            return false;
        }

        return true;
    }

    @Override
    public boolean insert(Map<String, String> map) {


        return false;
    }

    @Override
    public boolean update(int id, Map<String, String> map) {
        return false;
    }
}
