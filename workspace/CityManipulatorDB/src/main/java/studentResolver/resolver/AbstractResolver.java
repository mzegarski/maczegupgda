package studentResolver.resolver;

import java.util.List;
import java.util.Map;

/**
 * Created by zegarski on 2017-07-06.
 */
public abstract class AbstractResolver<T> {

    public abstract List<T> get();

    public abstract T get(int id);

    public abstract boolean delete(int id);

    public abstract boolean insert (Map<String, String> map);

    public abstract boolean update (int id, Map<String, String> map);


}
