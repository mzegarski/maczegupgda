package mutable.types;

public class MutableDouble {

	private double value;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public MutableDouble(double value) {
		super();
		this.value = value;
	}
	
	public void increment (){
		
		value++;
	}
	
	
	
}
