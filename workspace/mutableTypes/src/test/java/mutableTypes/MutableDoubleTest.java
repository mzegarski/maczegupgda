package mutableTypes;

import org.junit.Test;

import mutable.types.MutableDouble;

public class MutableDoubleTest {

	@Test
	public void test() {

		MutableDouble a = new MutableDouble(10.2);
		MutableDouble b = new MutableDouble(100.1);
		
		b = a;

		a.setValue(20.6);
		
		assert a.getValue() == 20.6;
		assert b.getValue() == 20.6;
		
		a.increment();
		
		assert a.getValue() == 21.6;

	}

}
