package mutableTypes;

import org.junit.Test;

public class DoubleTest {

@Test
	public void test() {

		double a = 10.5;
		double b = a;

		a = 7.1;

		assert a == 7.1;
		assert b == 10.5;
	}

}
