package sortowanie;

import java.util.Random;

public class Sortuj {

	public static void main(String[] args) {

		// masz 10 milion�w liczb 10 bitowych, posortuj je liniowo

		int howManyNumbersToSort = 10000000;
		int[] numbers = new int[howManyNumbersToSort];
		
		Random generator = new Random();
		int howBigNumbersAre = 1024; // 2^10
		for (int i = 0; i < howManyNumbersToSort; i++)
			numbers[i] = generator.nextInt(howBigNumbersAre);

		int[] sortedNumbers = new int[howBigNumbersAre];

		for (int i = 0; i < howManyNumbersToSort; i++)
			sortedNumbers[numbers[i]]++;

		for (int i = 0; i < howBigNumbersAre; i++)
			System.out.println("Liczba " + i + " wyst�puje w tablicy " + sortedNumbers[i] + " razy.");

	}

}
