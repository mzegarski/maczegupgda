package userGenerator;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {

	private final String path = "src/main/resources/";
	private User currentUser;

	public User getRandomUser() {

		UserSex us = UserSex.SEX_MALE;
		if (new Random().nextInt(2) == 0) {
			us = UserSex.SEX_FEMALE;
		}

		return getRandomUser(us);
	}

	public User getRandomUser(UserSex sex) {

		currentUser = new User();

		currentUser.setUserSex(sex);

		currentUser.setSecondName(validateSecondNameByGender(sex));

		currentUser.setAddress(generateRandomAddress());

		currentUser.setPhone(generateRandomPhoneNumber());

		currentUser.setCcn(getRandomCreditCardNumber());

		currentUser.setBirthDate(getRandomBirthDate());

		currentUser.setPESEL(generatePESEL(sex));

		return currentUser;
	}

	private String validateSecondNameByGender(UserSex sex) {
		String name = "";
		if (sex.equals(UserSex.SEX_MALE))
			name = getRandomLineFromFile("name_m.txt");
		else
			name = getRandomLineFromFile("name_f.txt");
		currentUser.setName(name);

		String secondName = getRandomLineFromFile("lastname.txt");

		if (sex.equals(UserSex.SEX_FEMALE))
			if (secondName.charAt(secondName.length() - 1) == 'i')
				secondName = secondName.substring(0, secondName.length() - 1) + "a";
		return secondName;
	}

	public boolean validatePESEL(String PESEL, UserSex sex) {

		int[] pesel = new int[11];

		for (int i = 0; i < 11; i++)

			pesel[i] = Integer.parseInt(PESEL.substring(i, i + 1));

		if (sex.toString() == "SEX_MALE") {
			if (pesel[9] % 2 != 0) {
				System.out.println("Chłop: Błąd na 10 liczbie w peselu, nie zgadza się z płcią");
				return false;
			}
		} else if (pesel[9] % 2 == 0) {
			System.out.println("Baba: Błąd na 10 liczbie w peselu, nie zgadza się z płcią");
			return false;
		}

		if (pesel[10] != ((9 * pesel[0] + 7 * pesel[1] + 3 * pesel[2] + pesel[3] + 9 * pesel[4] + 7 * pesel[5]
				+ 3 * pesel[6] + pesel[7] + 9 * pesel[8] + 7 * pesel[9]) % 10)) {
			System.out.println("Nie zgadza się liczba kontrolna!");
			return false;
		}

		return true;
	}

	private String generatePESEL(UserSex sex) {
		String PESEL = "";

		String[] bday = currentUser.getBirthDate().split("\\.");

		if (Integer.parseInt(bday[2]) >= 1800 && Integer.parseInt(bday[2]) <= 1899)
			bday[1] += 80;
		if (Integer.parseInt(bday[2]) >= 2000 && Integer.parseInt(bday[2]) <= 2099)
			bday[1] += 20;

		PESEL = bday[2].substring(2, 4) + bday[1] + bday[0];

		int[] peselTab = new int[11];

		for (int i = 0; i <= 5; i++) {
			peselTab[i] = Integer.parseInt(PESEL.substring(i, i + 1));
		}

		peselTab[6] = new Random().nextInt(9);
		peselTab[7] = new Random().nextInt(9);
		peselTab[8] = new Random().nextInt(9);

		if (sex.equals(UserSex.SEX_MALE))

			peselTab[9] = new Random().nextInt(4) * 2;

		else

			peselTab[9] = (new Random().nextInt(4) * 2) + 1;

		// 9×a + 7×b + 3×c + 1×d + 9×e + 7×f + 3×g + 1×h + 9×i + 7×j

		peselTab[10] = ((9 * peselTab[0] + 7 * peselTab[1] + 3 * peselTab[2] + peselTab[3] + 9 * peselTab[4]
				+ 7 * peselTab[5] + 3 * peselTab[6] + peselTab[7] + 9 * peselTab[8] + 7 * peselTab[9]) % 10);
		PESEL = "";

		for (int i = 0; i < 11; i++)
			PESEL += peselTab[i];
		return PESEL;
	}

	private Date getRandomBirthDate() {
		String date = getRandomDate();

		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

		try {
			Date bdate = sdf.parse(date);

			return bdate;
		} catch (ParseException e1) {

			e1.printStackTrace();
		}

		return null;
	}

	private String getRandomDate() {
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		if (year % 4 == 0) {
			daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return leadZero(day) + "." + leadZero(month) + "." + year; // dd.MM.YYYY;
	}

	private String leadZero(int arg) {
		if (arg < 10)
			return "0" + arg;
		else
			return "" + arg;
	}

	private String getRandomCreditCardNumber() {
		String card = "";

		for (int i = 0; i < 16; i++)
			card += new Random().nextInt(9);

		card = card.substring(0, 4) + "-" + card.substring(4, 8) + "-" + card.substring(8, 12) + "-"
				+ card.substring(12, card.length());
		return card;
	}

	private String generateRandomPhoneNumber() {
		String phone = "";

		for (int i = 0; i < 9; i++)
			phone += new Random().nextInt(9);

		phone = "+48 " + phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6, phone.length());
		return phone;
	}

	private Address generateRandomAddress() {
		Address adr = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split("\t");
		adr.setCity(cityData[0]);
		adr.setZipcode(cityData[1]);
		adr.setCountry("Poland");
		adr.setStreet(getRandomLineFromFile("street.txt"));
		adr.setNumber("" + (new Random().nextInt(100)));
		return adr;
	}

	private int countLines(String filename) {

		int lines = 0;

		try (Scanner sc = new Scanner(new File(path + filename))) {

			while (sc.hasNextLine()) {

				lines++;
				sc.nextLine();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return lines;

	}

	private String getRandomLineFromFile(String filename) {

		Random generator = new Random();

		int randomLine = generator.nextInt(countLines(filename));
		String currentLine = "";
		try (Scanner sc = new Scanner(new File(path + filename))) {

			for (int i = 0; i < randomLine; i++)
				sc.nextLine();

			currentLine = sc.nextLine();

			return currentLine;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return currentLine;
	}

}
