package userGenerator;

public enum UserSex {

	SEX_MALE("Mężczyzna"), SEX_FEMALE("Kobieta"), SEX_UNDEFINED("Nieokreślono");

	private String name;

	private UserSex(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
