//Zaprojektuj struktur� danych dla przyk�adowego u�ytkownika. 
//Struktura powinna deklarowa� pola tj. imi�, nazwisko, adres, telefon, numer karty kredytowej, pesel, date urodzenia. 
//Przygotuj plik imiona.txt, nazwiska.txt, ulice.txt, kt�re b�d� zawiera�y przyk�adowe dane. 
//Nast�pnie w klasie dostarcz metody, kt�re wygeneruj� losowe dane, losowego u�ytkownika. 
//Generator numeru pesel powinien generowa� pesel, kt�ry przedzie walidacj� (tj. poprawny numer).

package userGenerator;

import java.text.SimpleDateFormat;
import java.util.Date;

public class User {

	private String name;
	private String secondName;
	private Address address;
	private String phone;
	private UserSex userSex;
	private String ccn;
	private String PESEL;
	private Date birthDate;

	public User() {

	}

	public User(String name, String secondName, Address address, String phone, UserSex userSex, String ccn,
			String pESEL, Date birthDate) {
		this.name = name;
		this.secondName = secondName;
		this.address = address;
		this.phone = phone;
		this.userSex = userSex;
		this.ccn = ccn;
		PESEL = pESEL;
		this.birthDate = birthDate;
	}

	@Override
	public String toString() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		
		
		return name + " " + secondName + ", " + address + ", tel: " + phone + ", " + userSex.getName() + ", Karta= "
				+ ccn + ", Data urodzenia: " + sdf.format(birthDate) + ", PESEL= " + PESEL;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public UserSex getUserSex() {
		return userSex;
	}

	public void setUserSex(UserSex userSex) {
		this.userSex = userSex;
	}

	public String getCcn() {
		return ccn;
	}

	public void setCcn(String ccn) {
		this.ccn = ccn;
	}

	public String getPESEL() {
		return PESEL;
	}

	public void setPESEL(String pESEL) {
		PESEL = pESEL;
	}

	public String getBirthDate() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		
		return sdf.format(birthDate);
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

}
