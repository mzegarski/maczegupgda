package userGenerator;

public class Main {

	public static void main(String[] args) {

		generateAndPrintUsers(50);

		//validateGeneratedPesels(50);

		System.exit(0);
	}

	public static void generateAndPrintUsers(int howMany) {
		UserGenerator ug = new UserGenerator();
		for (int i = 0; i < howMany; i++)
			System.out.println(ug.getRandomUser());
	}

	public static void validateGeneratedPesels(int howMany) {

		UserGenerator ug = new UserGenerator();
		User[] users = new User[howMany];
		for (int i = 0; i < howMany; i++)
			users[i] = ug.getRandomUser();
		for (int i = 0; i < howMany; i++)
			ug.validatePESEL(users[i].getPESEL(), users[i].getUserSex());
	}

}
