package animalsInterface;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class animalMain {

	public static void main(String[] args) {
		AnimalInterface[] animals = { new Cat(), new Dog() };

		for (AnimalInterface animal : animals) {
			animal.saySize();
		}
		
		save5NoisesToFile("saysmth.txt", new Cat());

	}

	public static void save5NoisesToFile(String filename, AnimalInterface animal) {
		try {
			PrintStream out = new PrintStream(filename);
			out.println(animal.saySize());
			out.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}

}
