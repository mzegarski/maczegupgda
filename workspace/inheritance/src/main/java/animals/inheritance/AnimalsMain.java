package animals.inheritance;

public class AnimalsMain {

	public static void main(String[] args) {
		Animals[] animals = { new Cat("Mruczek"), new Dog("Burek"), new Llama("Lamus"), new Turtle("Szybcior"),
				new Cat("Puszek"), new Dog("Azor") };

		for (Animals x : animals) {

			x.introduce();
			x.noise();
			
		}
	}

}
