package animals.inheritance;

public class Llama extends Animals implements AnimalInterface {

	public Llama(String name) {
		super(name);
	}

	public void noise() {

		System.out.println("Die, die.");
	}

	@Override
	public void saySize() {
		System.out.println("I'm big.");

	}
}
