package animals.inheritance;

public class Cat extends Animals implements AnimalInterface {

	public Cat(String name) {
		super(name);
	}

	public void noise() {

		System.out.println("Miau, miau.");
	}

	@Override
	public void saySize() {
		System.out.println("I'm small.");

	}

}
