package animals.inheritance;

public class Turtle extends Animals implements AnimalInterface {

	public Turtle(String name) {
		super(name);
	}

	public void noise() {

		System.out.println("Hm, hm.");
	}

	@Override
	public void saySize() {
		System.out.println("I come in different sizes, I'm weird like that.");

	}

}
