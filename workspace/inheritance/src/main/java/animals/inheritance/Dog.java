package animals.inheritance;

public class Dog extends Animals implements AnimalInterface {

	public Dog(String name) {
		super(name);
	}

	public void noise() {

		System.out.println("Hau, hau.");
	}

	@Override
	public void saySize() {
		System.out.println("I'm medium.");

	}

}
