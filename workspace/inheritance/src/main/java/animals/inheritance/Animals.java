package animals.inheritance;

public abstract class Animals {

	private String name;

	public String getName() {
		return name;
	}

	public Animals(String name) {
		this.name = name;
	}

	public void introduce() {

		System.out.println("I'm a " + this.getClass().getSimpleName() + ". My name is: " + this.name);

	}

	public abstract void noise();

}
