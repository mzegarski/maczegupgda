package family.inheritance;

public class Family {

	public static void main(String[] args) {

		FamilyMember m = new Mother("Gra�yna");
		// typ referencji Family Member, zmienna referencyjna m, typ obiektu
		// Mother
		FamilyMember f = new Father("Jacek");
		FamilyMember s = new Son("Jakub");
		FamilyMember d = new Daugther("Kinga");

		FamilyMember[] allFamilyMembers = { m, f, s, d };

		// FamilyMember[] allFamilyMembers = { new Mother("Gra�yna"), new
		// Father("Jacek"), new Son("Jakub"),
		// new Daugther("Kinga") };

		for (FamilyMember members : allFamilyMembers)
			members.introduce();

	}

}
