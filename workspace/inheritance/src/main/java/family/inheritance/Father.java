package family.inheritance;

public class Father extends FamilyMember {

	public Father(String name) {
		super(name);
	}

	@Override
	public void introduce() {

		System.out.println("I'm a " + this.getClass().getSimpleName() + ". My name is: " + this.getName());
	}
}
