package family.inheritance;

public class Son extends FamilyMember {

	public Son(String name) {
		super(name);
	}
	
	@Override
	public void introduce() {

		System.out.println("I'm a " + this.getClass().getSimpleName() + ". My name is: " + this.getName());
	}
}
