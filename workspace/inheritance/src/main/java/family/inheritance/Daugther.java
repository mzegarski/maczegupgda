package family.inheritance;

public class Daugther extends FamilyMember {

	public Daugther(String name) {
		super(name);
	}

	@Override
	public void introduce() {

		System.out.println("I'm a " + this.getClass().getSimpleName() + ". My name is: " + this.getName());
	}
	
}
