package family;

public class Family {

	public static void main(String[] args) {

		Mother m = new Mother("Anna");
		Father f = new Father("Jacek");
		Son s = new Son("Jakub");
		Daugther d = new Daugther("Kinga");

		introduce(m);
		introduce(f);
		introduce(s);
		introduce(d);

	}

	public static void introduce(Mother mother) {

		System.out.println("My name is: " + mother.getName());
	}

	public static void introduce(Father father) {

		System.out.println("My name is: " + father.getName());
	}

	public static void introduce(Son son) {

		System.out.println("My name is: " + son.getName());
	}

	public static void introduce(Daugther daugther) {

		System.out.println("My name is: " + daugther.getName());
	}

}
