package FiguresInterface;

public class Circle implements Figure {

	private double size;

	public Circle(int size) {
		super();
		this.size = size;
	}

	@Override
	public double[] getSize() {

		double[] allSize = { this.size };

		return allSize;
	}

	@Override
	public double countArea() {

		return Math.PI * size * size;
	}

	@Override
	public double countCircumference() {

		return 2 * Math.PI * size;

	}

}
