package FiguresInterface;

public class Square implements Figure {

	private double size;

	public Square(int size) {
		super();
		this.size = size;
	}

	@Override
	public double[] getSize() {

		double[] allSize = { this.size };

		return allSize;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public double countArea() {

		return size * size;
	}

	@Override
	public double countCircumference() {

		return 4 * size;

	}

}
