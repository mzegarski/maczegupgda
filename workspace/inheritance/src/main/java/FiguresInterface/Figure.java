package FiguresInterface;

public interface Figure {

	double countArea();

	double countCircumference();

	double[] getSize();

}
