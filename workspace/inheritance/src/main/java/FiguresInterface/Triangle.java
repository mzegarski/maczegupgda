package FiguresInterface;

public class Triangle implements Figure {

	private double size1, size2, size3;

	public Triangle(double size1, double size2, double size3) {
		super();
		this.size1 = size1;
		this.size2 = size2;
		this.size3 = size3;
	}

	@Override
	public double[] getSize() {

		double[] allSize = { this.size1, this.size2, this.size3 };

		return allSize;
	}


	@Override
	public double countArea() {

		double p = this.countCircumference() * 0.5;

		return Math.sqrt(p * (p - size1) * (p - size2) * (p - size3));

	}

	@Override
	public double countCircumference() {

		return size1 + size2 + size3;
	}

	

}
