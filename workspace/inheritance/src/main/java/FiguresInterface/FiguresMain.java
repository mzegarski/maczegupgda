package FiguresInterface;

public class FiguresMain {

	public static void main(String[] args) {

		Figure s = new Square(5);
		Figure c = new Circle(5);
		Figure t = new Triangle(2, 2, 2);

		System.out.println("Pole kwadratu o boku " + s.getSize()[0] + " to: " + s.countArea());
		System.out.println("Obw�d kwadratu o boku " + s.getSize()[0] + " to: " + s.countCircumference());

		System.out.println("Pole ko�a o promieniu " + c.getSize()[0] + " to: " + c.countArea());
		System.out.println("Obw�d ko�a o promieniu " + c.getSize()[0] + " to: " + c.countCircumference());

		System.out.println("Pole tr�jk�ta o bokach " + t.getSize()[0] + ", " + t.getSize()[1] + ", " + t.getSize()[2]
				+ " to: " + t.countArea());
		System.out.println("Obw�d tr�jk�ta o bokach " + t.getSize()[0] + ", " + t.getSize()[1] + ", " + t.getSize()[2]
				+ " to: " + t.countCircumference());

	}

}
