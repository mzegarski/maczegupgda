package teddybear;

import java.util.Scanner;

public class Bear {

	private String imie;

	public Bear(String imie) {
		
		this.imie = imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getImie() {
		return imie;
	}

}
