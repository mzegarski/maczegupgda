package quadraticEquation;

public class QuadraticEquation {

	private double a, b, c;

	public QuadraticEquation(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public double delta() {
		return b * b - (4 * a * c);
	}

	public double X1() {

		return (-1 * b + Math.sqrt(delta())) / (2 * a);
	}

	public double X2() {

		return (-1 * b - Math.sqrt(delta())) / (2 * a);
	}
}
