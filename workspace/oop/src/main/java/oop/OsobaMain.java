package oop;

import java.util.Scanner;

public class OsobaMain {

	public static void main(String[] args) {
		Osoba jan /* referencja obiektu */ = new Osoba("Jan", 23); // Tworzenie
																	// instancji
																	// obiektu

		jan.przedstawSie();

		jan.setImie("Janek");

		jan.przedstawSie();

		System.out.println("Moje imie to: " + jan.getImie());

	}

}
