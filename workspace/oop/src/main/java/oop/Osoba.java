package oop;

//klasa

public class Osoba {

	// Stan
	private String imie;
	private int wiek;

	public Osoba(String imie, int wiek) {
		// System.out.println("Kreator osoby");
		this.imie = imie;
		this.wiek = wiek;
	}

	// ZACHOWANIA:
	public void przedstawSie() {
		System.out.println(imie + " (" + wiek + ")");
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public void setWiek(int wiek) {
		this.wiek = wiek;
	}

	public String getImie() {
		return imie;
	}

	public int getWiek() {
		return wiek;
	}

}
