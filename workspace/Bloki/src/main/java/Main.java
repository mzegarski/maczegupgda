import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// printAdressess("Algorytmiczna", 3);

		guessNumber();

	}

	public static void printAdressess(String street, int number) {

		char cage = 'A';

		for (int block = 1; block <= number; block += 2)
			for (int apartment = 1; apartment <= 12; apartment++) {
				if (apartment > 6)
					cage = 'B';
				System.out.println("ul. " + street + " " + block + cage + " mieszkanie: " + apartment);
			}
	}

	public static boolean guessNumber() {

		Random r = new Random();

		int secretNumber = r.nextInt(10);

		int tries = 10;
		System.out.println("SN: " + secretNumber);

		try (Scanner sc = new Scanner(System.in)) {

			do {
				System.out.println("Zgadnij liczbe: ");
				int guess = sc.nextInt();
				if (guess == secretNumber) {
					System.out.println("Trafione!");
					return true;
				}

				System.out.println("Nie trafione.");
				tries--;
				System.out.println("Pozostalo prob: " + tries);
			} while (tries > 0);

		} catch (Exception e) {
			System.out.println("Jakiś blad scannera: " + e);
		}

		System.out.println("Dupa.");
		return false;

	}

}