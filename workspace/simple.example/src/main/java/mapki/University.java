package mapki;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class University {

	private Map<Integer, Student> students = new HashMap<>();

	public void addStudent(int indexNumber, String name, String surname) {
		students.put(indexNumber, new Student(indexNumber, name, surname));
	}

	public void addStudent(String name, String surname) {
		Random indexGenerator = new Random();
		int indexNumber;
		do {

			indexNumber = 100000 + indexGenerator.nextInt(1000);
		} while (studentEist(indexNumber) != false);
		students.put(indexNumber, new Student(indexNumber, name, surname));
	}

	public boolean studentEist(int indexNumber) {
		return students.containsKey(indexNumber);
	}

	public Student getStudent(int indexNumber) {
		return students.get(indexNumber);
	}

	public int numOfStudents() {
		return students.size();
	}

	public void showAllStudents() {
		for (Student s : students.values())
			System.out.println(s.getIndexNum() + " : " + s.getName() + " " + s.getSurname());
	}

}
