package mapki;

public class Student {

	private int indexNum;
	private String name;
	private String surname;

	public Student(int indexNum, String name, String surname) {
		super();
		this.indexNum = indexNum;
		this.name = name;
		this.surname = surname;
	}

	public int getIndexNum() {
		return indexNum;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

}
