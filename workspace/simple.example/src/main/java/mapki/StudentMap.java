package mapki;

import java.util.HashMap;
import java.util.Map;

public class StudentMap {

	public static void main(String[] args) {

		Map<Integer, Student> students = new HashMap<>();

		students.put(1, new Student(100100, "Jan", "Kowalski"));
		students.put(2, new Student(100321, "Kuba", "Szofer"));
		students.put(3, new Student(100432, "Maciej", "Kubek"));
		students.put(4, new Student(100423, "Ala", "My�liciel"));
		students.put(5, new Student(100234, "Anna", "Nowak"));
		
		for (Student student : students.values()){
			System.out.println(student.getName());
		}
		
	
		// sprawd�, czy mapa zawiera studenta o indeksie 100200,
		for (Student student : students.values())
			System.out.println(student.getIndexNum() == 100100);
				
		
		
	//System.out.println(students.get(1));
		
		// wypisz studenta o indeksie 100400
		
		for (Student student : students.values())
			if (student.getIndexNum() == 100100)
				System.out.println(student.getName() +" ma index numer " +student.getIndexNum());
		
		// wypisz liczb� student�w
		
		// wypisz wszystkich student�w
		
		// zmie� implementacje na TreeMap

	}

}
