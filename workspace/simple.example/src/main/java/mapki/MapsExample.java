package mapki;

import java.util.HashMap;
import java.util.Map;

public class MapsExample {

	public static void main(String[] args) {

		mapki();

	}

	private static void mapki() {
		Map<String, String> dictionary = new HashMap<>();
		dictionary.put("Kot", "Cat");
		dictionary.put("Pies", "Dog");

		System.out.println("Klucze:");
		for (String key : dictionary.keySet()) {

			System.out.println(key);
		}

		System.out.println("Values:");
		for (String value : dictionary.values()) {

			System.out.println(value);
		}

		System.out.println("Pary");
		for (Map.Entry<String, String> pair : dictionary.entrySet()) {
			System.out.println(pair);
			// System.out.println(pair.getKey() +"->" +pair.getValue());
		}
		dictionary.remove("Pies");
		System.out.println("Usuna�em jeden klucz: ");
		for (Map.Entry<String, String> pair : dictionary.entrySet()) {
			System.out.println(pair);
			// System.out.println(pair.getKey() +"->" +pair.getValue());
		}
		
		System.out.println(dictionary.get("Kot"));
	}
}
