package mapki;

public class UniversityExample {

	public static void main(String[] args) {

		University univ = new University();

		univ.addStudent("Anna", "Kowalska");
		univ.addStudent("Paul", "Walker");
		univ.addStudent("John", "Wayne");
		univ.addStudent("Bruce", "Willis");
		univ.addStudent("Ignacy", "Krasicki");
		univ.addStudent(100600, "Anna", "Kowalska");
		univ.addStudent(100777, "Anna", "Kowalska");

		System.out.println(univ.studentEist(100600));
		System.out.println(univ.studentEist(100999));

		System.out.println(univ.numOfStudents());

		univ.showAllStudents();

	}

}
