package kolejki;

public class Person implements Comparable <Person> {

	private final String name;
	private final int age;

	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {

		return "" + name + " (" + age + ")";
	}

	
	public int compareTo(Person o) {

		return this.age - o.age;

		// return ((this.age > o.age) ? 1 : -1);

	}

}
