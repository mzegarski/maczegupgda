package kolejki;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class QueueExample {

	public static void main(String[] args) {

		// Queue<Integer> q1 = new LinkedList<>();
		Queue<Integer> q1 = new PriorityQueue<>();

		q1.add(1);
		q1.add(21);
		q1.add(51);
		q1.add(11);
		q1.add(231);
		q1.add(1231);
		q1.add(3);

		while (!q1.isEmpty()) {
			System.out.println(q1.poll());
		}

		System.out.println(q1.poll());
	}

}
