package kolejki;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class PersonQueueExample {

	public static void main(String[] args) {

		Queue<Person> personQ = new PriorityQueue<>(new Comparator<Person>() {
			// klasa anonimowa
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		personQ.offer(new Person("Maciej", 25));
		personQ.offer(new Person("Jakub", 55));
		personQ.offer(new Person("Kasia", 88));
		personQ.offer(new Person("Adam", 30));
		personQ.offer(new Person("Kasia", 88));

		// for (Person person : personQ) {
		//
		// System.out.println(person);
		//
		// }

		while (!personQ.isEmpty()) {

			System.out.println(personQ.poll());

		}

	}
}
