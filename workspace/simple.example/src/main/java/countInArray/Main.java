package countInArray;

import java.util.Random;

public class Main {

	// Metoda zlicza ilo�� wyst�pie� liczb w tablicy ( ile-jakiej )

	public static void main(String[] args) {

		int n = 100; // zmienna dla ilo�ci liczb w tablicy i ich wielko�ci

		int[] intArray1 = new int[n];
		Random generator = new Random();
		for (int i = 0; i < n; i++) {

			intArray1[i] = generator.nextInt(n);

		}

		int[] intArray2 = new int[10];
		for (int i = 0; i < 10; i++) {
			intArray2[i] = 5;
		}

		System.out.println(countInArray(intArray2, 5));

	}

	public static int countInArray(int[] intArray, int numberInArray) {
		int howManyTimes = 0;

		for (int i : intArray) {

			if (i == numberInArray)
				howManyTimes++;

		}

		return howManyTimes;
	}

}
