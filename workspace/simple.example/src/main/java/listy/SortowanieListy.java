package listy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import kolejki.Person;

public class SortowanieListy {

	public static void main(String[] args) {

		List<Person> personL = new ArrayList<>();

		personL.add(new Person("Jakub", 55));
		personL.add(new Person("Maciej", 25));
		personL.add(new Person("Kasia", 88));
		personL.add(new Person("Adam", 30));
		personL.add(new Person("Kasia", 88));

		System.out.println("Przed sortowaniem");

		for (Person person : personL) {
			System.out.println(person);
		}

		Collections.sort(personL);

		System.out.println("Po sortowaniem");

		for (Person person : personL) {
			System.out.println(person);
		}
		
		
		System.out.println("Po shufflowaniu");
		Collections.shuffle(personL);
		for (Person person : personL) {
			System.out.println(person);
		}

	}
}
