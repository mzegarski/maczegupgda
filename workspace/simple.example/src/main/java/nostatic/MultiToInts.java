package nostatic;

public class MultiToInts extends TwoInts {
	private int amount;
	
	public MultiToInts(int a, int b, int amount) {
		super(a, b);
		this.amount = amount;
	}

	@Override
	public int add() {
		return super.add()*amount;
	}
}
