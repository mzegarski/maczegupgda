package simple.example;

public class StringBuilderExample {

	public static void main(String[] args) {

		// int n = 4600;
		// long start = System.currentTimeMillis();
		// for (int i = 0; i < n; i++) {
		// // generateA(n);
		// generateByBuilder(n);
		// }
		// System.out.println(System.currentTimeMillis() - start);

		drawRectangleByBuilder(3, 10);

	}

	private static void drawRectangleByBuilder(int width, int hight) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < hight; i++)
			drawLineByBuilder(width, builder);

		System.out.println(builder.toString());
	}

	private static void drawLineByBuilder(int width, StringBuilder builder) {
		for (int j = 0; j < width; j++)
			builder.append('*');

		builder.append('\n');
	}

	private static void generateByBuilder(int n) {
		StringBuilder builer = new StringBuilder();
		for (int i = 0; i < n; i++)
			builer.append('a');
		// System.out.println(builer.toString());
	}

	private static void generateA(int n) {
		String a = "";
		for (int i = 0; i < n; i++)
			a += "a";
		// System.out.println(a);
	}

}
