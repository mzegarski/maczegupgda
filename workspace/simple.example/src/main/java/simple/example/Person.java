package simple.example;

import java.util.Random;

public class Person {
	
	private String name;
	private String surname;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Person(String name, String surname) {
		super();
		this.name = name;
		this.surname = surname;
	}
	
	public Person(){
		
		
	}
	
//	public Person randomPerson(){
//		
//		String[] names ={"Adam","Jan"};
//		String[] surnames ={"Kowalski", "Nowak"};
//		
//		Random r = new Random();
//		
//		return (names[r.nextInt(2)] ,  surnames[r.nextInt(2)]);
//		
//		
//		
//	}
	
	

}
