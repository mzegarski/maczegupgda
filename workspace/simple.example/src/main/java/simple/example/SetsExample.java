package simple.example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetsExample {
	public static void main(String[] args) {

		Set<PairOfNum> setOfPairs = new HashSet<>();

		// trzeba w klasie PairOfNum wygenerowac hashtag i equals inaczej beda
		// sie powtarzac te same elementy zbioru

		setOfPairs.add(new PairOfNum(1, 2));
		setOfPairs.add(new PairOfNum(2, 1));
		setOfPairs.add(new PairOfNum(1, 1));
		setOfPairs.add(new PairOfNum(1, 2));

		for (PairOfNum i : setOfPairs)
			System.out.println(i);

		// setyOne();

	}

	private static void setyOne() {
		int[] tab = { 2000, 2000, 2000, 2000, 5000, 6000, 7000, 8000, 9000, 0 };

		Set<Integer> set = new HashSet<>();

		for (int i = 0; i < tab.length; i++) {
			set.add(tab[i]);
		}

		Set<Integer> set2 = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0));

		System.out.println(set);
		// System.out.println(set2);

		set.remove(2000);
		set.remove(0);

		System.out.println(set);

		System.out.println("Iterable: ");
		// Iterable<Integer> iterable = set;
		// for (int i : iterable) System.out.println(i);
		System.out.println("Jeste� tutaj");
		// Iterator <Integer> iterator = set.iterator();
		// System.out.println(iterator.next());
		System.out.println(set.iterator().next());

		System.out.println("has.next");
		Iterator<Integer> it = set.iterator();

		while (it.hasNext()) {

			System.out.println(it.next());
		}
	}

}
