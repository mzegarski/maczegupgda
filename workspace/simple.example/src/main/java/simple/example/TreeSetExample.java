package simple.example;

import java.util.Arrays;
import java.util.TreeSet;

public class TreeSetExample {

	public static void main(String[] args) {

		TreeSet<Integer> treeset = new TreeSet<>(Arrays.asList(900, 800, 700, 600, 500, 400, 300, 200, 100, 0));

		System.out.println(treeset);

		System.out.println("Ca�o��:");
		for (int i : treeset)
			System.out.println(i);

		System.out.println("Headset 300");
		for (int i : treeset.headSet(300))
			System.out.println(i);

		System.out.println("Tailset 300");
		for (int i : treeset.tailSet(300))
			System.out.println(i);

	}

}
