package simple.example;

public class Matrix {

	public static void main(String[] args) {

		int[][] m = { { 1, 2, 3 }, { 2, 2, 3 }, { 5, 5 }, null };

		// System.out.println(suma(m[0]));

		for (int w : sumy(m)) {
			System.out.println(w);
		}

	}

	public static int[] sumy(int[][] matrix) {
		int[] sumy = new int[matrix.length];

		for (int i = 0; i < matrix.length; i++) {

			sumy[i] += suma(matrix[i]);

		}

		return sumy;

	}

	public static int suma(int[] matrix) {
		int suma = 0;
		for (int m : matrix) {
			suma += m;
		}

		return suma;

	}

	public static void print(Integer[][] matrix) {

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}

	}

}
