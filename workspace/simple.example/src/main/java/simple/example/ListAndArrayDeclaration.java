package simple.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListAndArrayDeclaration {

	public static void main(String[] args) {
		// tablice();

		// szybko, ale tablica nie moze byc edytowana
		List<Integer> lista1 = Arrays.asList(1, 2, 3);
		// moze byc edytowana
		List<Integer> lista2 = new ArrayList<>(Arrays.asList(6, 6, 6));
		//Tradycyjnie
		//List<Integer> lista3 = new ArrayList<>;

	}

	private static void tablice() {
		// najprostsze deklarowanie i inicjalizowanie tablicy gdy mamy zmienne
		int[] tab1 = { 1, 2, 3, 4 };
		int[] tab2 = null;
		// jezeli jest zadeklarowana to trzeba tak
		tab2 = new int[] { 1, 2 };
		// albo tak
		tab2 = new int[2];
		tab2[0] = 1;
		tab2[1] = 2;

		int[][] tab2dim1 = { { 1, 2 }, { 2 }, { 3, 4, 5, } };
		// tablica zer
		int[][] tab2dim2 = new int[4][3];
		// tablica null
		int[][] tab2dim3 = new int[4][];
	}

}
